﻿CREATE FUNCTION [dim].[Get_DataSourceId]
(
	@DataSource_Tag		VARCHAR(24)
)
RETURNS INT
AS
BEGIN

	DECLARE @DataSourceId	INT;

	SELECT TOP 1
		@DataSourceId	= [l].[DS_DataSourceId]
	FROM 
		[dim].[DS_DataSource_LookUp] [l]
	WHERE
		[l].[DS_Tag]	= @DataSource_Tag;

	RETURN @DataSourceId;

END;