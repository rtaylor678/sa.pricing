﻿CREATE FUNCTION [dim].[Get_CommodityId]
(
	@Commodity_Tag		VARCHAR(12)
)
RETURNS INT
AS
BEGIN

	DECLARE @CommodityId	INT;

	SELECT TOP 1
		@CommodityId	= [l].[CO_CommodityId]
	FROM 
		[dim].[CO_Commodity_LookUp] [l]
	WHERE
		[l].[CO_Tag]	= @Commodity_Tag;

	RETURN @CommodityId;

END;