﻿CREATE TABLE [dim].[LO_Location_Bridge] (
    [LO_MethodologyId] INT                NOT NULL,
    [LO_LocationId]    INT                NOT NULL,
    [LO_DescendantId]  INT                NOT NULL,
    [LO_PositedBy]     INT                CONSTRAINT [DF__LO_Location_Bridge_LO_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [LO_PositedAt]     DATETIMEOFFSET (7) CONSTRAINT [DF__LO_Location_Bridge_LO_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [LO_RowGuid]       UNIQUEIDENTIFIER   CONSTRAINT [DF__LO_Location_Bridge_LO_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__LO_Location_Bridge] PRIMARY KEY CLUSTERED ([LO_MethodologyId] ASC, [LO_LocationId] ASC, [LO_DescendantId] ASC),
    CONSTRAINT [FK__LO_Location_Bridge_LO_DescendantId] FOREIGN KEY ([LO_DescendantId]) REFERENCES [dim].[LO_Location_LookUp] ([LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Bridge_LO_Location_Parent_Ancestor] FOREIGN KEY ([LO_MethodologyId], [LO_LocationId]) REFERENCES [dim].[LO_Location_Parent] ([LO_MethodologyId], [LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Bridge_LO_Location_Parent_Descendant] FOREIGN KEY ([LO_MethodologyId], [LO_LocationId]) REFERENCES [dim].[LO_Location_Parent] ([LO_MethodologyId], [LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Bridge_LO_LocationId] FOREIGN KEY ([LO_LocationId]) REFERENCES [dim].[LO_Location_LookUp] ([LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Bridge_LO_PositedBy] FOREIGN KEY ([LO_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UX__LO_Location_Bridge_LO_RowGuid] UNIQUE NONCLUSTERED ([LO_RowGuid] ASC)
);

