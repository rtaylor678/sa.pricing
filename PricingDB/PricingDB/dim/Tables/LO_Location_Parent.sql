﻿CREATE TABLE [dim].[LO_Location_Parent] (
    [LO_MethodologyId] INT                 NOT NULL,
    [LO_LocationId]    INT                 NOT NULL,
    [LO_ParentId]      INT                 NOT NULL,
    [LO_Operator]      CHAR (1)            CONSTRAINT [DF__LO_Location_Parent_LO_Operator] DEFAULT ('+') NOT NULL,
    [LO_SortKey]       INT                 NOT NULL,
    [LO_Hierarchy]     [sys].[hierarchyid] NOT NULL,
    [LO_PositedBy]     INT                 CONSTRAINT [DF__LO_Location_Parent_LO_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [LO_PositedAt]     DATETIMEOFFSET (7)  CONSTRAINT [DF__LO_Location_Parent_LO_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [LO_RowGuid]       UNIQUEIDENTIFIER    CONSTRAINT [DF__LO_Location_Parent_LO_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__LO_Location_Parent] PRIMARY KEY CLUSTERED ([LO_MethodologyId] DESC, [LO_LocationId] ASC),
    CONSTRAINT [FK__LO_Location_Parent_LO_LocationId] FOREIGN KEY ([LO_LocationId]) REFERENCES [dim].[LO_Location_LookUp] ([LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Parent_LO_Operator] FOREIGN KEY ([LO_Operator]) REFERENCES [dim].[OP_Operator_LookUp] ([OP_OperatorId]),
    CONSTRAINT [FK__LO_Location_Parent_LO_Parent] FOREIGN KEY ([LO_ParentId]) REFERENCES [dim].[LO_Location_LookUp] ([LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Parent_LO_ParentParent] FOREIGN KEY ([LO_MethodologyId], [LO_LocationId]) REFERENCES [dim].[LO_Location_Parent] ([LO_MethodologyId], [LO_LocationId]),
    CONSTRAINT [FK__LO_Location_Parent_LO_PositedBy] FOREIGN KEY ([LO_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UX__LO_Location_Parent_LO_RowGuid] UNIQUE NONCLUSTERED ([LO_RowGuid] ASC)
);

