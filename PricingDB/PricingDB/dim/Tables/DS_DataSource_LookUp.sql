﻿CREATE TABLE [dim].[DS_DataSource_LookUp] (
    [DS_DataSourceId] INT                IDENTITY (1, 1) NOT NULL,
    [DS_Tag]          VARCHAR (24)       NOT NULL,
    [DS_Name]         NVARCHAR (48)      NOT NULL,
    [DS_Detail]       NVARCHAR (96)      NOT NULL,
    [DS_PositedBy]    INT                CONSTRAINT [DF__DS_DataSource_LookUp_DS_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [DS_PositedAt]    DATETIMEOFFSET (7) CONSTRAINT [DF__DS_DataSource_LookUp_DS_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [DS_RowGuid]      UNIQUEIDENTIFIER   CONSTRAINT [DF__DS_DataSource_LookUp_DS_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__DS_DataSource_LookUp] PRIMARY KEY NONCLUSTERED ([DS_DataSourceId] ASC),
    CONSTRAINT [CL__DS_DataSource_LookUp_DS_Detail] CHECK ([DS_Detail]<>''),
    CONSTRAINT [CL__DS_DataSource_LookUp_DS_Name] CHECK ([DS_Name]<>''),
    CONSTRAINT [CL__DS_DataSource_LookUp_DS_Tag] CHECK ([DS_Tag]<>''),
    CONSTRAINT [FK__DS_DataSource_LookUp_DS_PositedBy] FOREIGN KEY ([DS_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__DS_DataSource_LookUp_DS_Tag] UNIQUE CLUSTERED ([DS_Tag] ASC),
    CONSTRAINT [UX__DS_DataSource_LookUp_DS_Detail] UNIQUE NONCLUSTERED ([DS_Detail] ASC),
    CONSTRAINT [UX__DS_DataSource_LookUp_DS_Name] UNIQUE NONCLUSTERED ([DS_Name] ASC),
    CONSTRAINT [UX__DS_DataSource_LookUp_DS_RowGuid] UNIQUE NONCLUSTERED ([DS_RowGuid] ASC)
);

