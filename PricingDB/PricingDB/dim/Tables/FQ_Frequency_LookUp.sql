﻿CREATE TABLE [dim].[FQ_Frequency_LookUp] (
    [FQ_FrequencyId] INT                IDENTITY (1, 1) NOT NULL,
    [FQ_Tag]         CHAR (2)           NOT NULL,
    [FQ_Name]        NVARCHAR (14)      NOT NULL,
    [FQ_Detail]      NVARCHAR (30)      NOT NULL,
    [FQ_PositedBy]   INT                CONSTRAINT [DF__FQ_Frequency_LookUp_FQ_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [FQ_PositedAt]   DATETIMEOFFSET (7) CONSTRAINT [DF__FQ_Frequency_LookUp_FQ_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [FQ_RowGuid]     UNIQUEIDENTIFIER   CONSTRAINT [DF__FQ_Frequency_LookUp_FQ_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__FQ_Frequency_LookUp] PRIMARY KEY NONCLUSTERED ([FQ_FrequencyId] ASC),
    CONSTRAINT [CL__FQ_Frequency_LookUp_FQ_Detail] CHECK ([FQ_Detail]<>''),
    CONSTRAINT [CL__FQ_Frequency_LookUp_FQ_Name] CHECK ([FQ_Name]<>''),
    CONSTRAINT [CL__FQ_Frequency_LookUp_FQ_Tag] CHECK ([FQ_Tag]<>''),
    CONSTRAINT [FK__FQ_Frequency_LookUp_FQ_PositedBy] FOREIGN KEY ([FQ_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__FQ_Frequency_LookUp_FQ_Tag] UNIQUE CLUSTERED ([FQ_Tag] ASC),
    CONSTRAINT [UX__FQ_Frequency_LookUp_FQ_Detail] UNIQUE NONCLUSTERED ([FQ_Detail] ASC),
    CONSTRAINT [UX__FQ_Frequency_LookUp_FQ_Name] UNIQUE NONCLUSTERED ([FQ_Name] ASC),
    CONSTRAINT [UX__FQ_Frequency_LookUp_FQ_RowGuid] UNIQUE NONCLUSTERED ([FQ_RowGuid] ASC)
);

