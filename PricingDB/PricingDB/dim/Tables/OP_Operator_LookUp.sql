﻿CREATE TABLE [dim].[OP_Operator_LookUp] (
    [OP_OperatorId] CHAR (1)           NOT NULL,
    [OP_PositedBy]  INT                CONSTRAINT [DF__OP_Operator_LookUp_OP_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [OP_PositedAt]  DATETIMEOFFSET (7) CONSTRAINT [DF__OP_Operator_LookUp_OP_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [OP_RowGuid]    UNIQUEIDENTIFIER   CONSTRAINT [DF__OP_Operator_LookUp_OP_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__OP_Operator_LookUp] PRIMARY KEY CLUSTERED ([OP_OperatorId] ASC),
    CONSTRAINT [CL__OP_Operator_LookUp_OP_OperatorId] CHECK ([OP_OperatorId]<>''),
    CONSTRAINT [FK__OP_Operator_LookUp_OP_PositedBy] FOREIGN KEY ([OP_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UX__OP_Operator_LookUp_OP_RowGuid] UNIQUE NONCLUSTERED ([OP_RowGuid] ASC)
);

