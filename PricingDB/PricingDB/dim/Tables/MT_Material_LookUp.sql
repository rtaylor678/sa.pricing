﻿CREATE TABLE [dim].[MT_Material_LookUp] (
    [MT_MaterialId] INT                IDENTITY (1, 1) NOT NULL,
    [MT_Tag]        VARCHAR (24)       NOT NULL,
    [MT_Name]       NVARCHAR (48)      NOT NULL,
    [MT_Detail]     NVARCHAR (192)     NOT NULL,
    [MT_PositedBy]  INT                CONSTRAINT [DF__MT_Material_LookUp_MT_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [MT_PositedAt]  DATETIMEOFFSET (7) CONSTRAINT [DF__MT_Material_LookUp_MT_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [MT_RowGuid]    UNIQUEIDENTIFIER   CONSTRAINT [DF__MT_Material_LookUp_MT_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__MT_Material_LookUp] PRIMARY KEY NONCLUSTERED ([MT_MaterialId] ASC),
    CONSTRAINT [CL__MT_Material_LookUp_MT_Detail] CHECK ([MT_Detail]<>''),
    CONSTRAINT [CL__MT_Material_LookUp_MT_Name] CHECK ([MT_Name]<>''),
    CONSTRAINT [CL__MT_Material_LookUp_MT_Tag] CHECK ([MT_Tag]<>''),
    CONSTRAINT [FK__MT_Material_LookUp_MT_PositedBy] FOREIGN KEY ([MT_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__MT_Material_LookUp_MT_Tag] UNIQUE CLUSTERED ([MT_Tag] ASC),
    CONSTRAINT [UX__MT_Material_LookUp_MT_Detail] UNIQUE NONCLUSTERED ([MT_Detail] ASC),
    CONSTRAINT [UX__MT_Material_LookUp_MT_Name] UNIQUE NONCLUSTERED ([MT_Name] ASC),
    CONSTRAINT [UX__MT_Material_LookUp_MT_RowGuid] UNIQUE NONCLUSTERED ([MT_RowGuid] ASC)
);

