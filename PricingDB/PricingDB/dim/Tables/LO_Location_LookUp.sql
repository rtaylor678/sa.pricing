﻿CREATE TABLE [dim].[LO_Location_LookUp] (
    [LO_LocationId] INT                IDENTITY (1, 1) NOT NULL,
    [LO_Tag]        VARCHAR (24)       NOT NULL,
    [LO_Name]       NVARCHAR (48)      NOT NULL,
    [LO_Detail]     NVARCHAR (192)     NOT NULL,
    [LO_PositedBy]  INT                CONSTRAINT [DF__LO_Location_LookUp_LO_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [LO_PositedAt]  DATETIMEOFFSET (7) CONSTRAINT [DF__LO_Location_LookUp_LO_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [LO_RowGuid]    UNIQUEIDENTIFIER   CONSTRAINT [DF__LO_Location_LookUp_LO_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__LO_Location_LookUp] PRIMARY KEY NONCLUSTERED ([LO_LocationId] ASC),
    CONSTRAINT [CL__LO_Location_LookUp_LO_Detail] CHECK ([LO_Detail]<>''),
    CONSTRAINT [CL__LO_Location_LookUp_LO_Name] CHECK ([LO_Name]<>''),
    CONSTRAINT [CL__LO_Location_LookUp_LO_Tag] CHECK ([LO_Tag]<>''),
    CONSTRAINT [FK__LO_Location_LookUp_LO_PositedBy] FOREIGN KEY ([LO_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__LO_Location_LookUp_LO_Tag] UNIQUE CLUSTERED ([LO_Tag] ASC),
    CONSTRAINT [UX__LO_Location_LookUp_LO_Detail] UNIQUE NONCLUSTERED ([LO_Detail] ASC),
    CONSTRAINT [UX__LO_Location_LookUp_LO_Name] UNIQUE NONCLUSTERED ([LO_Name] ASC),
    CONSTRAINT [UX__LO_Location_LookUp_LO_RowGuid] UNIQUE NONCLUSTERED ([LO_RowGuid] ASC)
);

