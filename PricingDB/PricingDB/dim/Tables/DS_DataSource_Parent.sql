﻿CREATE TABLE [dim].[DS_DataSource_Parent] (
    [DS_MethodologyId] INT                 NOT NULL,
    [DS_DataSourceId]  INT                 NOT NULL,
    [DS_ParentId]      INT                 NOT NULL,
    [DS_Operator]      CHAR (1)            CONSTRAINT [DF__DS_DataSource_Parent_DS_Operator] DEFAULT ('+') NOT NULL,
    [DS_SortKey]       INT                 NOT NULL,
    [DS_Hierarchy]     [sys].[hierarchyid] NOT NULL,
    [DS_PositedBy]     INT                 CONSTRAINT [DF__DS_DataSource_Parent_DS_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [DS_PositedAt]     DATETIMEOFFSET (7)  CONSTRAINT [DF__DS_DataSource_Parent_DS_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [DS_RowGuid]       UNIQUEIDENTIFIER    CONSTRAINT [DF__DS_DataSource_Parent_DS_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__DS_DataSource_Parent] PRIMARY KEY CLUSTERED ([DS_MethodologyId] DESC, [DS_DataSourceId] ASC),
    CONSTRAINT [FK__DS_DataSource_Parent_DS_DataSourceId] FOREIGN KEY ([DS_DataSourceId]) REFERENCES [dim].[DS_DataSource_LookUp] ([DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Parent_DS_Operator] FOREIGN KEY ([DS_Operator]) REFERENCES [dim].[OP_Operator_LookUp] ([OP_OperatorId]),
    CONSTRAINT [FK__DS_DataSource_Parent_DS_Parent] FOREIGN KEY ([DS_ParentId]) REFERENCES [dim].[DS_DataSource_LookUp] ([DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Parent_DS_ParentParent] FOREIGN KEY ([DS_MethodologyId], [DS_DataSourceId]) REFERENCES [dim].[DS_DataSource_Parent] ([DS_MethodologyId], [DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Parent_DS_PositedBy] FOREIGN KEY ([DS_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UX__DS_DataSource_Parent_DS_RowGuid] UNIQUE NONCLUSTERED ([DS_RowGuid] ASC)
);

