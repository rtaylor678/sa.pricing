﻿CREATE TABLE [dim].[UM_Uom_LookUp] (
    [UM_UomId]     INT                IDENTITY (1, 1) NOT NULL,
    [UM_Tag]       VARCHAR (12)       NOT NULL,
    [UM_Name]      NVARCHAR (24)      NOT NULL,
    [UM_Detail]    NVARCHAR (24)      NOT NULL,
    [UM_Glyph]     NVARCHAR (5)       NULL,
    [UM_Factor]    FLOAT (53)         NULL,
    [UM_PositedBy] INT                CONSTRAINT [DF__UM_Uom_LookUp_UM_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [UM_PositedAt] DATETIMEOFFSET (7) CONSTRAINT [DF__UM_Uom_LookUp_UM_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [UM_RowGuid]   UNIQUEIDENTIFIER   CONSTRAINT [DF__UM_Uom_LookUp_UM_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__UM_Uom_LookUp] PRIMARY KEY NONCLUSTERED ([UM_UomId] ASC),
    CONSTRAINT [CL__UM_Uom_LookUp_UM_Detail] CHECK ([UM_Detail]<>''),
    CONSTRAINT [CL__UM_Uom_LookUp_UM_Glyph] CHECK ([UM_Glyph]<>''),
    CONSTRAINT [CL__UM_Uom_LookUp_UM_Name] CHECK ([UM_Name]<>''),
    CONSTRAINT [CL__UM_Uom_LookUp_UM_Tag] CHECK ([UM_Tag]<>''),
    CONSTRAINT [CR__UM_Uom_LookUp_UM_Factor] CHECK ([UM_Factor]>(0.0)),
    CONSTRAINT [FK__UM_Uom_LookUp_UM_PositedBy] FOREIGN KEY ([UM_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__UM_Uom_LookUp_UM_Tag] UNIQUE CLUSTERED ([UM_Tag] ASC),
    CONSTRAINT [UX__UM_Uom_LookUp_UM_Detail] UNIQUE NONCLUSTERED ([UM_Detail] ASC),
    CONSTRAINT [UX__UM_Uom_LookUp_UM_Name] UNIQUE NONCLUSTERED ([UM_Name] ASC),
    CONSTRAINT [UX__UM_Uom_LookUp_UM_RowGuid] UNIQUE NONCLUSTERED ([UM_RowGuid] ASC)
);

