﻿CREATE VIEW [dim].[LO_Location]
WITH SCHEMABINDING
AS
SELECT
	[b].[LO_MethodologyId],
	[b].[LO_LocationId],
	[a].[LO_SortKey],
	[a].[LO_Hierarchy],
	[b].[LO_DescendantId],
	[d].[LO_Operator]
FROM [dim].[LO_Location_Bridge]			[b]
INNER JOIN [dim].[LO_Location_Parent]	[a]
	ON	[a].[LO_MethodologyId]	= [b].[LO_MethodologyId]
	AND	[a].[LO_LocationId]		= [b].[LO_LocationId]
INNER JOIN [dim].[LO_Location_Parent]	[d]
	ON	[d].[LO_MethodologyId]	= [b].[LO_MethodologyId]
	AND	[d].[LO_LocationId]		= [b].[LO_DescendantId];