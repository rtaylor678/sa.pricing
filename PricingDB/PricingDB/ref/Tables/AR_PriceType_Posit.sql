﻿CREATE TABLE [ref].[AR_PriceType_Posit] (
    [AR_ID]               INT                IDENTITY (1, 1) NOT NULL,
    [AR_PriceTypeId]      TINYINT            NOT NULL,
    [AR_Description]      VARCHAR (26)       NOT NULL,
    [AR_ChangedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__AR_PriceType_Posit_AR_ChangedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [AR_PositedBy]        INT                CONSTRAINT [DF__AR_PriceType_Posit_AR_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [AR_PositedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__AR_PriceType_Posit_AR_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [AR_PositReliability] TINYINT            CONSTRAINT [DF__AR_PriceType_Posit_AR_Reliability] DEFAULT ((50)) NOT NULL,
    [AR_PositReliable]    AS                 (CONVERT([bit],case when [AR_PositReliability]>(0) then (1) else (0) end)) PERSISTED NOT NULL,
    [AR_RowGuid]          UNIQUEIDENTIFIER   CONSTRAINT [DF__AR_PriceType_Posit_AR_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__AR_PriceType_Posit] PRIMARY KEY NONCLUSTERED ([AR_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [CL__AR_PriceType_Posit_AR_Description] CHECK ([AR_Description]<>''),
    CONSTRAINT [FK__AR_PriceType_Posit_AR_PositedBy] FOREIGN KEY ([AR_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [IX__AR_PriceType_Posit] UNIQUE NONCLUSTERED ([AR_Description] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UK__AR_PriceType_Posit] UNIQUE CLUSTERED ([AR_PriceTypeId] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UX__AR_PriceType_Posit_AR_RowGuid] UNIQUE NONCLUSTERED ([AR_RowGuid] ASC)
);

