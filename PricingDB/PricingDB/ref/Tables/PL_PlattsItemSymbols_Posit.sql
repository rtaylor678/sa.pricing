﻿CREATE TABLE [ref].[PL_PlattsItemSymbols_Posit] (
    [PL_ID]               INT                IDENTITY (1, 1) NOT NULL,
    [PL_Category]         VARCHAR (3)        NOT NULL,
    [PL_Symbol]           CHAR (7)           NOT NULL,
    [PL_Bate]             CHAR (1)           NOT NULL,
    [PL_SymbolBate]       AS                 (CONVERT([char](8),[PL_Symbol]+[PL_Bate])) PERSISTED NOT NULL,
    [PL_Frequency]        CHAR (2)           NOT NULL,
    [PL_Currency]         CHAR (3)           NOT NULL,
    [PL_Uom]              VARCHAR (3)        NOT NULL,
    [PL_Earliest]         DATE               NULL,
    [PL_Latest]           DATE               NULL,
    [PL_Detail]           VARCHAR (192)      NOT NULL,
    [PL_ChangedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_ChangedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PL_PositedBy]        INT                CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [PL_PositedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PL_PositReliability] TINYINT            CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PL_Reliability] DEFAULT ((50)) NOT NULL,
    [PL_PositReliable]    AS                 (CONVERT([bit],case when [PL_PositReliability]>(0) then (1) else (0) end)) PERSISTED NOT NULL,
    [PL_RowGuid]          UNIQUEIDENTIFIER   CONSTRAINT [DF__PL_PlattsItemSymbols_Posit_PP_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__PL_PlattsItemSymbols_Posit] PRIMARY KEY NONCLUSTERED ([PL_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Bate] CHECK ([PL_Bate]<>''),
    CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Category] CHECK ([PL_Category]<>''),
    CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Curr] CHECK ([PL_Currency]<>''),
    CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Freq] CHECK ([PL_Frequency]<>''),
    CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Symbol] CHECK ([PL_Symbol]<>''),
    CONSTRAINT [CL__PL_PlattsItemSymbols_Posit_PL_Uom] CHECK ([PL_Uom]<>''),
    CONSTRAINT [CR__PL_PlattsItemSymbols_Posit_PL_DateRange] CHECK ([PL_Earliest]<=[PL_Latest]),
    CONSTRAINT [CR__PL_PlattsItemSymbols_Posit_PL_Detail] CHECK ([PL_Detail]<>''),
    CONSTRAINT [FK__PL_PlattsItemSymbols_Posit_PL_PositedBy] FOREIGN KEY ([PL_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__PL_PlattsItemSymbols_Posit] UNIQUE CLUSTERED ([PL_Symbol] ASC, [PL_Bate] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UX__PL_PlattsItemSymbols_Posit_PP_RowGuid] UNIQUE NONCLUSTERED ([PL_RowGuid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX__PL_PlattsItemSymbols_Posit_PL_Uom]
    ON [ref].[PL_PlattsItemSymbols_Posit]([PL_Uom] ASC)
    INCLUDE([PL_Symbol], [PL_Currency]);


GO
CREATE NONCLUSTERED INDEX [IX__PL_PlattsItemSymbols_Posit_PL_Currency]
    ON [ref].[PL_PlattsItemSymbols_Posit]([PL_Currency] ASC)
    INCLUDE([PL_Symbol], [PL_Uom]);

