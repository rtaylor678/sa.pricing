﻿CREATE TABLE [ref].[AR_Category_Posit] (
    [AR_ID]               INT                IDENTITY (1, 1) NOT NULL,
    [AR_Code]             CHAR (9)           NOT NULL,
    [AR_DisplayName]      VARCHAR (128)      NOT NULL,
    [AR_Category]         NVARCHAR (128)     NOT NULL,
    [AR_ChangedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__AR_Category_Posit_AR_ChangedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [AR_PositedBy]        INT                CONSTRAINT [DF__AR_Category_Posit_AR_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [AR_PositedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__AR_Category_Posit_AR_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [AR_PositReliability] TINYINT            CONSTRAINT [DF__AR_Category_Posit_AR_Reliability] DEFAULT ((50)) NOT NULL,
    [AR_PositReliable]    AS                 (CONVERT([bit],case when [AR_PositReliability]>(0) then (1) else (0) end)) PERSISTED NOT NULL,
    [AR_RowGuid]          UNIQUEIDENTIFIER   CONSTRAINT [DF__AR_Category_Posit_AR_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__AR_Category_Posit] PRIMARY KEY NONCLUSTERED ([AR_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [CL__AR_Category_Posit_AR_Category] CHECK ([AR_Category]<>''),
    CONSTRAINT [CL__AR_Category_Posit_AR_Code] CHECK ([AR_Code]<>''),
    CONSTRAINT [CL__AR_Category_Posit_AR_DisplayName] CHECK ([AR_DisplayName]<>''),
    CONSTRAINT [FK__AR_Category_Posit_AR_PositedBy] FOREIGN KEY ([AR_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__AR_Category_Posit] UNIQUE CLUSTERED ([AR_Code] ASC, [AR_Category] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UX__AR_Category_Posit_AR_RowGuid] UNIQUE NONCLUSTERED ([AR_RowGuid] ASC)
);

