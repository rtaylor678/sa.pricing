﻿CREATE FUNCTION [fact].[PR_TRD_PricingCommodity_Trade_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[PR_TRD_ID],
	[a].[PR_TRD_PositedBy],
	[a].[PR_TRD_PositedAt],
	[a].[PR_TRD_PositReliability],
	[a].[PR_TRD_PositReliable]
FROM
    [fact].[PR_TRD_PricingCommodity_Trade_Annex]		[a]
WHERE	[a].[PR_TRD_PositedAt]		<  @PositedBefore
	AND	[a].[PR_TRD_PositReliable]	= 1;