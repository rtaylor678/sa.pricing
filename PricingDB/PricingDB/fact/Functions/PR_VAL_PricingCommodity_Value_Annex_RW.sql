﻿CREATE FUNCTION [fact].[PR_VAL_PricingCommodity_Value_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[PR_VAL_ID],
	[a].[PR_VAL_PositedBy],
	[a].[PR_VAL_PositedAt],
	[a].[PR_VAL_PositReliability],
	[a].[PR_VAL_PositReliable]
FROM
    [fact].[PR_VAL_PricingCommodity_Value_Annex]		[a]
WHERE	[a].[PR_VAL_PositedAt]		<  @PositedBefore
	AND	[a].[PR_VAL_PositReliable]	= 1;