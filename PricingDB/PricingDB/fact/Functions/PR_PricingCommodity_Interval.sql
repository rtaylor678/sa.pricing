﻿CREATE FUNCTION [fact].[PR_PricingCommodity_Interval]
(
	@ChangedAfter	DATETIMEOFFSET(7)	=   '01-01-01',
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[z].[PR_ID],
	[z].[PR_CommodityId],
	[z].[PR_PriceDate],
	[z].[PR_FrequencyId],
	[z].[PR_CurrencyId],
	[z].[PR_UomId],
	[z].[PR_DataSourceId],

	[val].[PR_VAL_ID],
	[val].[PR_VAL_PR_ID],
	[val].[PR_VAL_Open],
	[val].[PR_VAL_Close],
	[val].[PR_VAL_Low],
	[val].[PR_VAL_High],
	[val].[PR_VAL_Volume],
	[val].[PR_VAL_ChangedAt],
	[val].[PR_VAL_PositedBy],
	[val].[PR_VAL_PositedAt],
	[val].[PR_VAL_PositReliability],
	[val].[PR_VAL_PositReliable],

	[trd].[PR_TRD_ID],
	[trd].[PR_TRD_PR_ID],
	[trd].[PR_TRD_Index],
	[trd].[PR_TRD_Mean],
	[trd].[PR_TRD_Ask],
	[trd].[PR_TRD_Bid],
	[trd].[PR_TRD_PrevInterest],
	[trd].[PR_TRD_Rate],
	[trd].[PR_TRD_Margin],
	[trd].[PR_TRD_DiffLow],
	[trd].[PR_TRD_DiffHigh],
	[trd].[PR_TRD_DiffIndex],
	[trd].[PR_TRD_DiffMidpoint],
	[trd].[PR_TRD_Midpoint],
	[trd].[PR_TRD_Netback],
	[trd].[PR_TRD_NetbackMargin],
	[trd].[PR_TRD_RGV],
	[trd].[PR_TRD_CumulativeIndex],
	[trd].[PR_TRD_CumulativeVolume],
	[trd].[PR_TRD_TransportCosts],
	[trd].[PR_TRD_ChangedAt],
	[trd].[PR_TRD_PositedBy],
	[trd].[PR_TRD_PositedAt],
	[trd].[PR_TRD_PositReliability],
	[trd].[PR_TRD_PositReliable]

FROM
	[fact].[PR_PricingCommodity]					[z]

LEFT OUTER JOIN
	[fact].[PR_VAL_PricingCommodity_Value_Interval](@ChangedAfter, @ChangedBefore, @PositedBefore)	[val]
		ON	[val].[PR_VAL_PR_ID]			= [z].[PR_ID]

LEFT OUTER JOIN
	[fact].[PR_TRD_PricingCommodity_Trade_Interval](@ChangedAfter, @ChangedBefore, @PositedBefore)	[trd]
		ON	[trd].[PR_TRD_PR_ID]			= [z].[PR_ID];