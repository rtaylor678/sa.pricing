﻿CREATE FUNCTION [fact].[PR_TRD_PricingCommodity_Trade_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PR_TRD_PR_ID],
	[p].[PR_TRD_ID],
	[p].[PR_TRD_ChangedAt],
	[a].[PR_TRD_PositedAt]
FROM
	[fact].[PR_TRD_PricingCommodity_Trade_Posit_RW](@ChangedBefore)				[p]
INNER JOIN
	[fact].[PR_TRD_PricingCommodity_Trade_Annex_RW](@PositedBefore)				[a]
		ON	[a].[PR_TRD_ID]			= [p].[PR_TRD_ID]
		AND	[a].[PR_TRD_PositedAt]	= (
			SELECT TOP 1
				[s].[PR_TRD_PositedAt]
			FROM
				[fact].[PR_TRD_PricingCommodity_Trade_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[PR_TRD_ID]		= [p].[PR_TRD_ID]
			ORDER BY
				[s].[PR_TRD_PositedAt]	DESC
			);