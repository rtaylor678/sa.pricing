﻿CREATE FUNCTION [fact].[PR_TRD_PricingCommodity_Trade_Interval]
(
	@ChangedAfter	DATETIMEOFFSET(7)	=   '01-01-01',
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PR_TRD_ID],
	[p].[PR_TRD_PR_ID],
	[p].[PR_TRD_Index],
	[p].[PR_TRD_Mean],
	[p].[PR_TRD_Ask],
	[p].[PR_TRD_Bid],
	[p].[PR_TRD_PrevInterest],
	[p].[PR_TRD_Rate],
	[p].[PR_TRD_Margin],
	[p].[PR_TRD_DiffLow],
	[p].[PR_TRD_DiffHigh],
	[p].[PR_TRD_DiffIndex],
	[p].[PR_TRD_DiffMidpoint],
	[p].[PR_TRD_Midpoint],
	[p].[PR_TRD_Netback],
	[p].[PR_TRD_NetbackMargin],
	[p].[PR_TRD_RGV],
	[p].[PR_TRD_CumulativeIndex],
	[p].[PR_TRD_CumulativeVolume],
	[p].[PR_TRD_TransportCosts],
	[p].[PR_TRD_ChangedAt],
	[a].[PR_TRD_PositedBy],
	[a].[PR_TRD_PositedAt],
	[a].[PR_TRD_PositReliability],
	[a].[PR_TRD_PositReliable]
FROM
	[fact].[PR_TRD_PricingCommodity_Trade_Posit_IN](@ChangedAfter, @ChangedBefore)	[p]
INNER JOIN
	[fact].[PR_TRD_PricingCommodity_Trade_Annex_RW](@PositedBefore)	[a]
		ON	[a].[PR_TRD_ID]			= [p].[PR_TRD_ID]
		AND	[a].[PR_TRD_PositedAt]	= (
			SELECT TOP 1
				[s].[PR_TRD_PositedAt]
			FROM
				[fact].[PR_TRD_PricingCommodity_Trade_RW](@ChangedBefore, @PositedBefore)		[s]
			WHERE
				[s].[PR_TRD_PR_ID]	= [p].[PR_TRD_PR_ID]
			ORDER BY
				[s].[PR_TRD_ChangedAt]	DESC,
				[s].[PR_TRD_PositedAt]	DESC
			);