﻿CREATE TABLE [fact].[PR_TRD_PricingCommodity_Trade_Annex] (
    [PR_TRD_ID]               INT                NOT NULL,
    [PR_TRD_PositedBy]        INT                CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [PR_TRD_PositedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PR_TRD_PositReliability] DECIMAL (5, 2)     CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_Reliability] DEFAULT ((0.0)) NOT NULL,
    [PR_TRD_PositReliable]    AS                 (CONVERT([bit],case when [PR_TRD_PositReliability]>(0.0) then (1) else (0) end)) PERSISTED NOT NULL,
    [PR_TRD_PositAssertion]   AS                 (CONVERT([char](1),coalesce(case when [PR_TRD_PositReliability]>(0.0) then '+' when [PR_TRD_PositReliability]<(0.0) then '-' else '?' end,'?'))) PERSISTED NOT NULL,
    [PR_TRD_RowGuid]          UNIQUEIDENTIFIER   CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__PR_TRD_PricingCommodity_Trade_Annex] PRIMARY KEY CLUSTERED ([PR_TRD_ID] ASC, [PR_TRD_PositedBy] ASC, [PR_TRD_PositedAt] DESC) WITH (FILLFACTOR = 95),
    CONSTRAINT [FK__PR_TRD_PricingCommodity_Trade_Annex_PositedBy] FOREIGN KEY ([PR_TRD_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [FK__PR_TRD_PricingCommodity_Trade_Annex_PR_TRD_ID] FOREIGN KEY ([PR_TRD_ID]) REFERENCES [fact].[PR_TRD_PricingCommodity_Trade_Posit] ([PR_TRD_ID]),
    CONSTRAINT [UX__PR_TRD_PricingCommodity_Trade_Annex_RowGuid] UNIQUE NONCLUSTERED ([PR_TRD_RowGuid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX__PR_TRD_PricingCommodity_Trade_Annex]
    ON [fact].[PR_TRD_PricingCommodity_Trade_Annex]([PR_TRD_PositedAt] DESC)
    INCLUDE([PR_TRD_ID], [PR_TRD_PositedBy], [PR_TRD_PositReliability], [PR_TRD_PositReliable]) WITH (FILLFACTOR = 95);


GO

CREATE TRIGGER [fact].[PR_TRD_PricingCommodity_Trade_Annex_Delete]
ON [fact].[PR_TRD_PricingCommodity_Trade_Annex]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_TRD_PricingCommodity_Trade_Annex].', 16, 1);
		ROLLBACK;
	END;

END;