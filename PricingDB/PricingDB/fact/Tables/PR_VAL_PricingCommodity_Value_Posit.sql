﻿CREATE TABLE [fact].[PR_VAL_PricingCommodity_Value_Posit] (
    [PR_VAL_ID]        INT                IDENTITY (1, 1) NOT NULL,
    [PR_VAL_PR_ID]     INT                NOT NULL,
    [PR_VAL_Open]      FLOAT (53)         NULL,
    [PR_VAL_Close]     FLOAT (53)         NULL,
    [PR_VAL_Low]       FLOAT (53)         NULL,
    [PR_VAL_High]      FLOAT (53)         NULL,
    [PR_VAL_Volume]    FLOAT (53)         NULL,
    [PR_VAL_Checksum]  AS                 (binary_checksum([PR_VAL_Open],[PR_VAL_Close],[PR_VAL_Low],[PR_VAL_High],[PR_VAL_Volume])) PERSISTED NOT NULL,
    [PR_VAL_ChangedAt] DATETIMEOFFSET (7) CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Posit_ChangedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PR_VAL_RowGuid]   UNIQUEIDENTIFIER   CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Posit_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__PR_VAL_PricingCommodity_Value_Posit] PRIMARY KEY NONCLUSTERED ([PR_VAL_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Posit_PR_ID] FOREIGN KEY ([PR_VAL_PR_ID]) REFERENCES [fact].[PR_PricingCommodity] ([PR_ID]),
    CONSTRAINT [UK__PR_VAL_PricingCommodity_Value_Posit] UNIQUE CLUSTERED ([PR_VAL_PR_ID] ASC, [PR_VAL_ChangedAt] DESC, [PR_VAL_Checksum] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UX__PR_VAL_PricingCommodity_Value_Posit_RowGuid] UNIQUE NONCLUSTERED ([PR_VAL_RowGuid] ASC)
);


GO

CREATE TRIGGER [fact].[PR_VAL_PricingCommodity_Value_Posit_Delete]
ON [fact].[PR_VAL_PricingCommodity_Value_Posit]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_VAL_PricingCommodity_Value_Posit].', 16, 1);
		ROLLBACK;
	END;

END;