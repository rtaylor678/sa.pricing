﻿CREATE TABLE [fact].[PR_PricingCommodity] (
    [PR_ID]           INT              IDENTITY (1, 1) NOT NULL,
    [PR_CommodityId]  INT              NOT NULL,
    [PR_PriceDate]    DATE             NOT NULL,
    [PR_FrequencyId]  INT              NOT NULL,
    [PR_CurrencyId]   INT              NOT NULL,
    [PR_UomId]        INT              NOT NULL,
    [PR_DataSourceId] INT              NOT NULL,
    [PR_RowGuid]      UNIQUEIDENTIFIER CONSTRAINT [DF__PR_PricingCommodity_PR_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__PR_PricingCommodity] PRIMARY KEY NONCLUSTERED ([PR_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [FK__PR_PricingCommodity_PR_CommodityId] FOREIGN KEY ([PR_CommodityId]) REFERENCES [dim].[CO_Commodity_LookUp] ([CO_CommodityId]),
    CONSTRAINT [FK__PR_PricingCommodity_PR_CurrencyId] FOREIGN KEY ([PR_CurrencyId]) REFERENCES [dim].[CU_Currency_LookUp] ([CU_CurrencyId]),
    CONSTRAINT [FK__PR_PricingCommodity_PR_DataSourceId] FOREIGN KEY ([PR_DataSourceId]) REFERENCES [dim].[DS_DataSource_LookUp] ([DS_DataSourceId]),
    CONSTRAINT [FK__PR_PricingCommodity_PR_FrequencyId] FOREIGN KEY ([PR_FrequencyId]) REFERENCES [dim].[FQ_Frequency_LookUp] ([FQ_FrequencyId]),
    CONSTRAINT [FK__PR_PricingCommodity_PR_UomId] FOREIGN KEY ([PR_UomId]) REFERENCES [dim].[UM_Uom_LookUp] ([UM_UomId]),
    CONSTRAINT [UK__PR_PricingCommodity] UNIQUE CLUSTERED ([PR_CommodityId] ASC, [PR_PriceDate] DESC, [PR_FrequencyId] ASC, [PR_CurrencyId] ASC, [PR_UomId] ASC, [PR_DataSourceId] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UX__PR_PricingCommodity_PR_RowGuid] UNIQUE NONCLUSTERED ([PR_RowGuid] ASC)
);


GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Delete]
ON [fact].[PR_PricingCommodity]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_PricingCommodity].', 16, 1);
		ROLLBACK;
	END;

END;