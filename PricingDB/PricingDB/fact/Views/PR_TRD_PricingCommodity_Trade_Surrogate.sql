﻿CREATE VIEW [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
WITH SCHEMABINDING
AS
SELECT
	[p].[PR_TRD_ID],
	[p].[PR_TRD_PR_ID],
	[p].[PR_TRD_Index],
	[p].[PR_TRD_Mean],
	[p].[PR_TRD_Ask],
	[p].[PR_TRD_Bid],
	[p].[PR_TRD_PrevInterest],
	[p].[PR_TRD_Rate],
	[p].[PR_TRD_Margin],
	[p].[PR_TRD_DiffLow],
	[p].[PR_TRD_DiffHigh],
	[p].[PR_TRD_DiffIndex],
	[p].[PR_TRD_DiffMidpoint],
	[p].[PR_TRD_Midpoint],
	[p].[PR_TRD_Netback],
	[p].[PR_TRD_NetbackMargin],
	[p].[PR_TRD_RGV],
	[p].[PR_TRD_CumulativeIndex],
	[p].[PR_TRD_CumulativeVolume],
	[p].[PR_TRD_TransportCosts],
	[p].[PR_TRD_ChangedAt],

	[a].[PR_TRD_PositedBy],
	[a].[PR_TRD_PositedAt],
	[a].[PR_TRD_PositReliability],
	[a].[PR_TRD_PositReliable]
FROM
	[fact].[PR_TRD_PricingCommodity_Trade_Posit]	[p]
INNER JOIN
	[fact].[PR_TRD_PricingCommodity_Trade_Annex]	[a]
		ON	[a].[PR_TRD_ID]		= [p].[PR_TRD_ID];