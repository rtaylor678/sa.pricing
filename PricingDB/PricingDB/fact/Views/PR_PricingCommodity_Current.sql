﻿CREATE VIEW [fact].[PR_PricingCommodity_Current]
WITH SCHEMABINDING
AS
SELECT
	[j].[PR_ID],
	[j].[PR_CommodityId],
	[j].[PR_PriceDate],
	[j].[PR_FrequencyId],
	[j].[PR_CurrencyId],
	[j].[PR_UomId],
	[j].[PR_DataSourceId],

	[j].[PR_VAL_ID],
	[j].[PR_VAL_PR_ID],
	[j].[PR_VAL_Open],
	[j].[PR_VAL_Close],
	[j].[PR_VAL_Low],
	[j].[PR_VAL_High],
	[j].[PR_VAL_Volume],
	[j].[PR_VAL_ChangedAt],
	[j].[PR_VAL_PositedBy],
	[j].[PR_VAL_PositedAt],
	[j].[PR_VAL_PositReliability],
	[j].[PR_VAL_PositReliable],

	[j].[PR_TRD_ID],
	[j].[PR_TRD_PR_ID],
	[j].[PR_TRD_Index],
	[j].[PR_TRD_Mean],
	[j].[PR_TRD_Ask],
	[j].[PR_TRD_Bid],
	[j].[PR_TRD_PrevInterest],
	[j].[PR_TRD_Rate],
	[j].[PR_TRD_Margin],
	[j].[PR_TRD_DiffLow],
	[j].[PR_TRD_DiffHigh],
	[j].[PR_TRD_DiffIndex],
	[j].[PR_TRD_DiffMidpoint],
	[j].[PR_TRD_Midpoint],
	[j].[PR_TRD_Netback],
	[j].[PR_TRD_NetbackMargin],
	[j].[PR_TRD_RGV],
	[j].[PR_TRD_CumulativeIndex],
	[j].[PR_TRD_CumulativeVolume],
	[j].[PR_TRD_TransportCosts],
	[j].[PR_TRD_ChangedAt],
	[j].[PR_TRD_PositedBy],
	[j].[PR_TRD_PositedAt],
	[j].[PR_TRD_PositReliability],
	[j].[PR_TRD_PositReliable]

FROM [fact].[PR_PricingCommodity_Interval](DEFAULT, DEFAULT, SYSDATETIMEOFFSET())	[j]
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Current_Insert]
ON [fact].[PR_PricingCommodity_Current]
INSTEAD OF INSERT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PositedAt	DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy	INT;
	EXECUTE @PositedBy	= [posit].[Return_PositorId];

	INSERT INTO [fact].[PR_PricingCommodity]
	(
		[PR_CommodityId],
		[PR_PriceDate],
		[PR_FrequencyId],
		[PR_CurrencyId],
		[PR_UomId],
		[PR_DataSourceId]
	)
	SELECT DISTINCT
		[i].[PR_CommodityId],
		[i].[PR_PriceDate],
		[i].[PR_FrequencyId],
		[i].[PR_CurrencyId],
		[i].[PR_UomId],
		[i].[PR_DataSourceId]
	FROM
		[inserted]											[i]
	LEFT OUTER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_FrequencyId]	= [i].[PR_FrequencyId]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	WHERE	[t].[PR_ID]	IS NULL;

	INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Posit]
	(
		[PR_VAL_PR_ID],
		[PR_VAL_Open],
		[PR_VAL_Close],
		[PR_VAL_Low],
		[PR_VAL_High],
		[PR_VAL_Volume],
		[PR_VAL_ChangedAt]
	)
	SELECT
		[t].[PR_ID],
		[i].[PR_VAL_Open],
		[i].[PR_VAL_Close],
		[i].[PR_VAL_Low],
		[i].[PR_VAL_High],
		[i].[PR_VAL_Volume],
			[PR_VAL_ChangedAt]	= COALESCE([i].[PR_VAL_ChangedAt], @PositedAt)
	FROM
		[inserted]											[i]
	INNER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_FrequencyId]	= [i].[PR_FrequencyId]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	LEFT OUTER JOIN
		[fact].[PR_VAL_PricingCommodity_Value_Posit]		[x]
			ON	[x].[PR_VAL_PR_ID]		= [t].[PR_ID]
			AND	[x].[PR_VAL_ChangedAt]	= COALESCE([i].[PR_VAL_ChangedAt], @PositedAt)
			AND	[x].[PR_VAL_Checksum]	= BINARY_CHECKSUM([i].[PR_VAL_Open], [i].[PR_VAL_Close], [i].[PR_VAL_Low], [i].[PR_VAL_High], [i].[PR_VAL_Volume])
	WHERE	[x].[PR_VAL_ID]			IS NULL
		AND(	[i].[PR_VAL_Open]	IS NOT NULL
			OR	[i].[PR_VAL_Close]	IS NOT NULL
			OR	[i].[PR_VAL_Low]	IS NOT NULL
			OR	[i].[PR_VAL_High]	IS NOT NULL
			OR	[i].[PR_VAL_Volume]	IS NOT NULL);

	INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Annex]
	(
		[PR_VAL_ID],
		[PR_VAL_PositedBy],
		[PR_VAL_PositedAt],
		[PR_VAL_PositReliability]
	)
	SELECT
		[p].[PR_VAL_ID],
			[PR_VAL_PositedBy]	= CASE WHEN (UPDATE([PR_VAL_PositedBy])) THEN [i].[PR_VAL_PositedBy] ELSE @PositedBy END,
			[PR_VAL_PositedAt]	= CASE WHEN (UPDATE([PR_VAL_PositedAt])) THEN [i].[PR_VAL_PositedAt] ELSE @PositedAt END,
		[i].[PR_VAL_PositReliability]
	FROM
		[inserted]											[i]
	INNER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_FrequencyId]	= [i].[PR_FrequencyId]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	INNER JOIN
		[fact].[PR_VAL_PricingCommodity_Value_Posit]		[p]
			ON	[p].[PR_VAL_PR_ID]		= [t].[PR_ID]
			AND	[p].[PR_VAL_ChangedAt]	= COALESCE([i].[PR_VAL_ChangedAt], @PositedAt)
	LEFT OUTER JOIN
		[fact].[PR_VAL_PricingCommodity_Value_Annex]		[x]
			ON	[x].[PR_VAL_ID]			= [p].[PR_VAL_ID]
			AND	[x].[PR_VAL_PositedBy]	= COALESCE([i].[PR_VAL_PositedBy], @PositedBy)
			AND	[x].[PR_VAL_PositedAt]	= COALESCE([i].[PR_VAL_PositedAt], @PositedAt)
	WHERE	[x].[PR_VAL_ID]			IS NULL
		AND(	[i].[PR_VAL_Open]	IS NOT NULL
			OR	[i].[PR_VAL_Close]	IS NOT NULL
			OR	[i].[PR_VAL_Low]	IS NOT NULL
			OR	[i].[PR_VAL_High]	IS NOT NULL
			OR	[i].[PR_VAL_Volume]	IS NOT NULL);

	INSERT INTO [fact].[PR_TRD_PricingCommodity_Trade_Posit]
	(
		[PR_TRD_PR_ID],
		[PR_TRD_Index],
		[PR_TRD_Mean],
		[PR_TRD_Ask],
		[PR_TRD_Bid],
		[PR_TRD_PrevInterest],
		[PR_TRD_Rate],
		[PR_TRD_Margin],
		[PR_TRD_DiffLow],
		[PR_TRD_DiffHigh],
		[PR_TRD_DiffIndex],
		[PR_TRD_DiffMidpoint],
		[PR_TRD_Midpoint],
		[PR_TRD_Netback],
		[PR_TRD_NetbackMargin],
		[PR_TRD_RGV],
		[PR_TRD_CumulativeIndex],
		[PR_TRD_CumulativeVolume],
		[PR_TRD_TransportCosts],
		[PR_TRD_ChangedAt]
	)
	SELECT
		[t].[PR_ID],
		[i].[PR_TRD_Index],
		[i].[PR_TRD_Mean],
		[i].[PR_TRD_Ask],
		[i].[PR_TRD_Bid],
		[i].[PR_TRD_PrevInterest],
		[i].[PR_TRD_Rate],
		[i].[PR_TRD_Margin],
		[i].[PR_TRD_DiffLow],
		[i].[PR_TRD_DiffHigh],
		[i].[PR_TRD_DiffIndex],
		[i].[PR_TRD_DiffMidpoint],
		[i].[PR_TRD_Midpoint],
		[i].[PR_TRD_Netback],
		[i].[PR_TRD_NetbackMargin],
		[i].[PR_TRD_RGV],
		[i].[PR_TRD_CumulativeIndex],
		[i].[PR_TRD_CumulativeVolume],
		[i].[PR_TRD_TransportCosts],
			[PR_TRD_ChangedAt]	= COALESCE([i].[PR_TRD_ChangedAt], @PositedAt)
	FROM
		[inserted]											[i]
	INNER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_FrequencyId]	= [i].[PR_FrequencyId]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	LEFT OUTER JOIN
		[fact].[PR_TRD_PricingCommodity_Trade_Posit]		[x]
			ON	[x].[PR_TRD_PR_ID]		= [t].[PR_ID]
			AND	[x].[PR_TRD_ChangedAt]	= COALESCE([i].[PR_TRD_ChangedAt], @PositedAt)
			AND	[x].[PR_TRD_Checksum]	= BINARY_CHECKSUM(
									[i].[PR_TRD_Index], [i].[PR_TRD_Mean], [i].[PR_TRD_Ask], [i].[PR_TRD_Bid],
									[i].[PR_TRD_PrevInterest], [i].[PR_TRD_Rate], [i].[PR_TRD_Margin], 
									[i].[PR_TRD_DiffLow], [i].[PR_TRD_DiffHigh], [i].[PR_TRD_DiffIndex], [i].[PR_TRD_DiffMidpoint], 
									[i].[PR_TRD_Midpoint], [i].[PR_TRD_Netback], [i].[PR_TRD_NetbackMargin], [i].[PR_TRD_RGV],
									[i].[PR_TRD_CumulativeIndex], [i].[PR_TRD_CumulativeVolume], [i].[PR_TRD_TransportCosts]
									)
	WHERE	[x].[PR_TRD_ID]			IS NULL
		AND(	[i].[PR_TRD_Index]				IS NOT NULL
			OR	[i].[PR_TRD_Mean]				IS NOT NULL
			OR	[i].[PR_TRD_Ask]				IS NOT NULL
			OR	[i].[PR_TRD_Bid]				IS NOT NULL
			OR	[i].[PR_TRD_PrevInterest]		IS NOT NULL
			OR	[i].[PR_TRD_Rate]				IS NOT NULL
			OR	[i].[PR_TRD_Margin]				IS NOT NULL
			OR	[i].[PR_TRD_DiffIndex]			IS NOT NULL
			OR	[i].[PR_TRD_DiffMidpoint]		IS NOT NULL
			OR	[i].[PR_TRD_DiffHigh]			IS NOT NULL
			OR	[i].[PR_TRD_DiffLow]			IS NOT NULL
			OR	[i].[PR_TRD_Midpoint]			IS NOT NULL
			OR	[i].[PR_TRD_Netback]			IS NOT NULL
			OR	[i].[PR_TRD_NetbackMargin]		IS NOT NULL
			OR	[i].[PR_TRD_RGV]				IS NOT NULL
			OR	[i].[PR_TRD_CumulativeIndex]	IS NOT NULL
			OR	[i].[PR_TRD_CumulativeVolume]	IS NOT NULL
			OR	[i].[PR_TRD_TransportCosts]		IS NOT NULL);

	INSERT INTO [fact].[PR_TRD_PricingCommodity_Trade_Annex]
	(
		[PR_TRD_ID],
		[PR_TRD_PositedBy],
		[PR_TRD_PositedAt],
		[PR_TRD_PositReliability]
	)
	SELECT
		[p].[PR_TRD_ID],
			[PR_TRD_PositedBy]	= CASE WHEN (UPDATE([PR_TRD_PositedBy])) THEN [i].[PR_TRD_PositedBy] ELSE @PositedBy END,
			[PR_TRD_PositedAt]	= CASE WHEN (UPDATE([PR_TRD_PositedAt])) THEN [i].[PR_TRD_PositedAt] ELSE @PositedAt END,
		[i].[PR_TRD_PositReliability]
	FROM
		[inserted]											[i]
	INNER JOIN
		[fact].[PR_PricingCommodity]						[t]
			ON	[t].[PR_CommodityId]	= [i].[PR_CommodityId]
			AND	[t].[PR_PriceDate]		= [i].[PR_PriceDate]
			AND	[t].[PR_FrequencyId]	= [i].[PR_FrequencyId]
			AND	[t].[PR_CurrencyId]		= [i].[PR_CurrencyId]
			AND	[t].[PR_UomId]			= [i].[PR_UomId]
			AND	[t].[PR_DataSourceId]	= [i].[PR_DataSourceId]
	INNER JOIN
		[fact].[PR_TRD_PricingCommodity_Trade_Posit]		[p]
			ON	[p].[PR_TRD_PR_ID]		= [t].[PR_ID]
			AND	[p].[PR_TRD_ChangedAt]	= COALESCE([i].[PR_TRD_ChangedAt], @PositedAt)
	LEFT OUTER JOIN
		[fact].[PR_TRD_PricingCommodity_Trade_Annex]		[x]
			ON	[x].[PR_TRD_ID]			= [p].[PR_TRD_ID]
			AND	[x].[PR_TRD_PositedBy]	= COALESCE([i].[PR_TRD_PositedBy], @PositedBy)
			AND	[x].[PR_TRD_PositedAt]	= COALESCE([i].[PR_TRD_PositedAt], @PositedAt)
	WHERE	[x].[PR_TRD_ID]			IS NULL
		AND(	[i].[PR_TRD_Index]				IS NOT NULL
			OR	[i].[PR_TRD_Mean]				IS NOT NULL
			OR	[i].[PR_TRD_Ask]				IS NOT NULL
			OR	[i].[PR_TRD_Bid]				IS NOT NULL
			OR	[i].[PR_TRD_PrevInterest]		IS NOT NULL
			OR	[i].[PR_TRD_Rate]				IS NOT NULL
			OR	[i].[PR_TRD_Margin]				IS NOT NULL
			OR	[i].[PR_TRD_DiffLow]			IS NOT NULL
			OR	[i].[PR_TRD_DiffHigh]			IS NOT NULL
			OR	[i].[PR_TRD_DiffIndex]			IS NOT NULL
			OR	[i].[PR_TRD_DiffMidpoint]		IS NOT NULL
			OR	[i].[PR_TRD_Midpoint]			IS NOT NULL
			OR	[i].[PR_TRD_Netback]			IS NOT NULL
			OR	[i].[PR_TRD_NetbackMargin]		IS NOT NULL
			OR	[i].[PR_TRD_RGV]				IS NOT NULL
			OR	[i].[PR_TRD_CumulativeIndex]	IS NOT NULL
			OR	[i].[PR_TRD_CumulativeVolume]	IS NOT NULL
			OR	[i].[PR_TRD_TransportCosts]		IS NOT NULL);

END;
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Current_Update]
ON [fact].[PR_PricingCommodity_Current]
INSTEAD OF UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	IF(UPDATE([PR_ID]))
		RAISERROR('The identity column [PR_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_CommodityId]))
		RAISERROR('The foreign key identity column [PR_CommodityId] is not updatable.', 16, 1);

	IF(UPDATE([PR_PriceDate]))
		RAISERROR('The identity column [PR_PriceDate] is not updatable.', 16, 1);

	IF(UPDATE([PR_FrequencyId]))
		RAISERROR('The foreign key identity column [PR_FrequencyId] is not updatable.', 16, 1);

	IF(UPDATE([PR_CurrencyId]))
		RAISERROR('The foreign key identity column [PR_CurrencyId] is not updatable.', 16, 1);

	IF(UPDATE([PR_UomId]))
		RAISERROR('The foreign key identity column [PR_UomId] is not updatable.', 16, 1);

	IF(UPDATE([PR_DataSourceId]))
		RAISERROR('The foreign key identity column [PR_DataSourceId] is not updatable.', 16, 1);

	IF(UPDATE([PR_VAL_ID]))
		RAISERROR('The identity column [PR_VAL_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_VAL_PR_ID]))
		RAISERROR('The identity column [PR_VAL_PR_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_VAL_PositReliable]))
		RAISERROR('The computed column [PR_VAL_PositReliable] is not updatable.', 16, 1);

	IF(UPDATE([PR_TRD_ID]))
		RAISERROR('The identity column [PR_TRD_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_TRD_PR_ID]))
		RAISERROR('The identity column [PR_TRD_PR_ID] is not updatable.', 16, 1);

	IF(UPDATE([PR_TRD_PositReliable]))
		RAISERROR('The computed column [PR_TRD_PositReliable] is not updatable.', 16, 1);

	DECLARE @PositedAt	DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy	INT;
	EXECUTE @PositedBy	= [posit].[Return_PositorId];

	IF (UPDATE([PR_VAL_Open])				OR
		UPDATE([PR_VAL_Close])				OR
		UPDATE([PR_VAL_Low])				OR
		UPDATE([PR_VAL_High])				OR
		UPDATE([PR_VAL_Volume])				OR
		UPDATE([PR_VAL_ChangedAt]))
	BEGIN

		INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Posit]
		(
			[PR_VAL_PR_ID],
			[PR_VAL_Open],
			[PR_VAL_Close],
			[PR_VAL_Low],
			[PR_VAL_High],
			[PR_VAL_Volume],
			[PR_VAL_ChangedAt]
		)
		SELECT
			[i].[PR_ID],
			[i].[PR_VAL_Open],
			[i].[PR_VAL_Close],
			[i].[PR_VAL_Low],
			[i].[PR_VAL_High],
			[i].[PR_VAL_Volume],
			[i].[PR_VAL_ChangedAt]
		FROM
			[inserted]											[i]
		LEFT OUTER JOIN
			[fact].[PR_VAL_PricingCommodity_Value_Posit]		[x]
				ON	[x].[PR_VAL_PR_ID]		= [i].[PR_ID]
				AND	[x].[PR_VAL_ChangedAt]	= [i].[PR_VAL_ChangedAt]
				AND	[x].[PR_VAL_Checksum]	= BINARY_CHECKSUM([i].[PR_VAL_Open], [i].[PR_VAL_Close], [i].[PR_VAL_Low], [i].[PR_VAL_High], [i].[PR_VAL_Volume])
		WHERE
			[x].[PR_VAL_ID]	IS NULL;

		INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Annex]
		(
			[PR_VAL_ID],
			[PR_VAL_PositedBy],
			[PR_VAL_PositedAt],
			[PR_VAL_PositReliability]
		)
		SELECT
			[p].[PR_VAL_ID],
				[PR_VAL_PositedBy]	= CASE WHEN (UPDATE([PR_VAL_PositedBy])) THEN [i].[PR_VAL_PositedBy] ELSE @PositedBy END,
				[PR_VAL_PositedAt]	= CASE WHEN (UPDATE([PR_VAL_PositedAt])) THEN [i].[PR_VAL_PositedAt] ELSE @PositedAt END,
			[i].[PR_VAL_PositReliability]
		FROM
			[inserted]										[i]
		INNER JOIN
			[fact].[PR_VAL_PricingCommodity_Value_Posit]	[p]
				ON	[p].[PR_VAL_PR_ID]		= [i].[PR_ID]
				AND	[p].[PR_VAL_ChangedAt]	= [i].[PR_VAL_ChangedAt]
		LEFT OUTER JOIN
			[fact].[PR_VAL_PricingCommodity_Value_Annex]	[a]
				ON	[a].[PR_VAL_ID]			= [p].[PR_VAL_ID]
		WHERE	[a].[PR_VAL_ID] IS NULL;

	END;

	IF (UPDATE([PR_TRD_Index])				OR
		UPDATE([PR_TRD_Mean])				OR
		UPDATE([PR_TRD_Ask])				OR
		UPDATE([PR_TRD_Bid])				OR
		UPDATE([PR_TRD_PrevInterest])		OR
		UPDATE([PR_TRD_Rate])				OR
		UPDATE([PR_TRD_Margin])				OR
		UPDATE([PR_TRD_DiffLow])			OR
		UPDATE([PR_TRD_DiffHigh])			OR
		UPDATE([PR_TRD_DiffIndex])			OR
		UPDATE([PR_TRD_DiffMidpoint])		OR
		UPDATE([PR_TRD_Midpoint])			OR
		UPDATE([PR_TRD_Netback])			OR
		UPDATE([PR_TRD_NetbackMargin])		OR
		UPDATE([PR_TRD_RGV])				OR
		UPDATE([PR_TRD_CumulativeIndex])	OR
		UPDATE([PR_TRD_CumulativeVolume])	OR
		UPDATE([PR_TRD_TransportCosts])		OR
		UPDATE([PR_TRD_ChangedAt]))
	BEGIN

		INSERT INTO [fact].[PR_TRD_PricingCommodity_Trade_Posit]
		(
			[PR_TRD_PR_ID],
			[PR_TRD_Index],
			[PR_TRD_Mean],
			[PR_TRD_Ask],
			[PR_TRD_Bid],
			[PR_TRD_PrevInterest],
			[PR_TRD_Rate],
			[PR_TRD_Margin],
			[PR_TRD_DiffLow],
			[PR_TRD_DiffHigh],
			[PR_TRD_DiffIndex],
			[PR_TRD_DiffMidpoint],
			[PR_TRD_Midpoint],
			[PR_TRD_Netback],
			[PR_TRD_NetbackMargin],
			[PR_TRD_RGV],
			[PR_TRD_CumulativeIndex],
			[PR_TRD_CumulativeVolume],
			[PR_TRD_TransportCosts],
			[PR_TRD_ChangedAt]
		)
		SELECT
			[i].[PR_ID],
			[i].[PR_TRD_Index],
			[i].[PR_TRD_Mean],
			[i].[PR_TRD_Ask],
			[i].[PR_TRD_Bid],
			[i].[PR_TRD_PrevInterest],
			[i].[PR_TRD_Rate],
			[i].[PR_TRD_Margin],
			[i].[PR_TRD_DiffLow],
			[i].[PR_TRD_DiffHigh],
			[i].[PR_TRD_DiffIndex],
			[i].[PR_TRD_DiffMidpoint],
			[i].[PR_TRD_Midpoint],
			[i].[PR_TRD_Netback],
			[i].[PR_TRD_NetbackMargin],
			[i].[PR_TRD_RGV],
			[i].[PR_TRD_CumulativeIndex],
			[i].[PR_TRD_CumulativeVolume],
			[i].[PR_TRD_TransportCosts],
			[i].[PR_TRD_ChangedAt]
		FROM
			[inserted]											[i]
		LEFT OUTER JOIN
			[fact].[PR_TRD_PricingCommodity_Trade_Posit]		[x]
				ON	[x].[PR_TRD_PR_ID]		= [i].[PR_ID]
				AND	[x].[PR_TRD_ChangedAt]	= [i].[PR_TRD_ChangedAt]
				AND	[x].[PR_TRD_Checksum]	= BINARY_CHECKSUM(
									[i].[PR_TRD_Index], [i].[PR_TRD_Mean], [i].[PR_TRD_Ask], [i].[PR_TRD_Bid],
									[i].[PR_TRD_PrevInterest], [i].[PR_TRD_Rate], [i].[PR_TRD_Margin], 
									[i].[PR_TRD_DiffLow], [i].[PR_TRD_DiffHigh], [i].[PR_TRD_DiffIndex], [i].[PR_TRD_DiffMidpoint],
									[i].[PR_TRD_Midpoint], [i].[PR_TRD_Netback], [i].[PR_TRD_NetbackMargin], [i].[PR_TRD_RGV],
									[i].[PR_TRD_CumulativeIndex], [i].[PR_TRD_CumulativeVolume], [i].[PR_TRD_TransportCosts]
									)
		WHERE
			[x].[PR_TRD_ID]	IS NULL;

		INSERT INTO [fact].[PR_TRD_PricingCommodity_Trade_Annex]
		(
			[PR_TRD_ID],
			[PR_TRD_PositedBy],
			[PR_TRD_PositedAt],
			[PR_TRD_PositReliability]
		)
		SELECT
			[p].[PR_TRD_ID],
				[PR_TRD_PositedBy]	= CASE WHEN (UPDATE([PR_TRD_PositedBy])) THEN [i].[PR_TRD_PositedBy] ELSE @PositedBy END,
				[PR_TRD_PositedAt]	= CASE WHEN (UPDATE([PR_TRD_PositedAt])) THEN [i].[PR_TRD_PositedAt] ELSE @PositedAt END,
			[i].[PR_TRD_PositReliability]
		FROM
			[inserted]										[i]
		INNER JOIN
			[fact].[PR_TRD_PricingCommodity_Trade_Posit]	[p]
				ON	[p].[PR_TRD_PR_ID]		= [i].[PR_ID]
				AND	[p].[PR_TRD_ChangedAt]	= [i].[PR_TRD_ChangedAt]
		LEFT OUTER JOIN
			[fact].[PR_TRD_PricingCommodity_Trade_Annex]	[a]
				ON	[a].[PR_TRD_ID]			= [p].[PR_TRD_ID]
		WHERE	[a].[PR_TRD_ID] IS NULL;

	END;

END;
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Current_Delete]
ON [fact].[PR_PricingCommodity_Current]
INSTEAD OF DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Now		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @PositedBy	INT;
	EXECUTE @PositedBy	= [posit].[Return_PositorId];

	INSERT INTO [fact].[PR_VAL_PricingCommodity_Value_Annex]
	(
		[PR_VAL_ID],
		[PR_VAL_PositedBy],
		[PR_VAL_PositedAt],
		[PR_VAL_PositReliability]
	)
	SELECT
		[d].[PR_VAL_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d];

	INSERT INTO [fact].[PR_TRD_PricingCommodity_Trade_Annex]
	(
		[PR_TRD_ID],
		[PR_TRD_PositedBy],
		[PR_TRD_PositedAt],
		[PR_TRD_PositReliability]
	)
	SELECT
		[d].[PR_TRD_ID],
		@PositedBy,
		@Now,
		0
	FROM
		[deleted]	[d];

END;