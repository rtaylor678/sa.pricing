﻿CREATE FUNCTION [etl].[AR_Unit_toUom]
(
	@DisplayName	VARCHAR(128),
	@Unit			VARCHAR(16)
)
RETURNS VARCHAR(24)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@i		INT	= CHARINDEX('/', @Unit);
	DECLARE	@Tag	VARCHAR(24) = CASE WHEN @i >= 1 THEN RIGHT(@Unit, LEN(@Unit) - @i) ELSE @Unit END;

	SET @Tag  = CASE WHEN @Unit IN ('bl/day', 'p/th') THEN @Unit ELSE @Tag END;
	set @tag = case when @tag = 't propane e' then 't' else @tag end; -- sw 6/10/16 to deal with this new case

	RETURN @Tag;

END;