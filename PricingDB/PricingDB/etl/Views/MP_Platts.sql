﻿CREATE VIEW [etl].[MP_Platts]
WITH SCHEMABINDING
AS
SELECT
	[p].[MP_ItemSymbol],
		[MP_CommodityId]			= [c].[CO_CommodityId],
	[p].[MP_FrequencyId],
	[p].[MP_CurrencyId],
	[p].[MP_CurrencyMultiplier],
	[p].[MP_UomId],
	[p].[MP_UomMultiplier]
FROM
	[etl].[MP_Platts_Posit]			[p]
INNER JOIN
	[dim].[CO_Commodity_LookUp]		[c]
		ON	[c].[CO_Tag]			= [p].[MP_ItemSymbol];
GO
CREATE UNIQUE CLUSTERED INDEX [PK__MP_Platts]
    ON [etl].[MP_Platts]([MP_ItemSymbol] ASC) WITH (FILLFACTOR = 95, STATISTICS_NORECOMPUTE = ON);

