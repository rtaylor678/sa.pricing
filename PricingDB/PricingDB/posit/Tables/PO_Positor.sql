﻿CREATE TABLE [posit].[PO_Positor] (
    [PO_PositorId]       INT                IDENTITY (1, 1) NOT NULL,
    [PO_PositorApp]      NVARCHAR (128)     CONSTRAINT [DF__PO_Positor_PO_PositorApp] DEFAULT (app_name()) NOT NULL,
    [PO_PositorName]     NVARCHAR (128)     CONSTRAINT [DF__PO_Positor_PO_PositorName] DEFAULT (suser_sname()) NOT NULL,
    [PO_PositorHost]     NVARCHAR (128)     CONSTRAINT [DF__PO_Positor_PO_PositorHost] DEFAULT (host_name()) NOT NULL,
    [PO_PositorSID]      VARBINARY (85)     CONSTRAINT [DF__PO_Positor_PO_PositorSID] DEFAULT (suser_sid()) NOT NULL,
    [PO_PositorUID]      INT                CONSTRAINT [DF__PO_Positor_PO_PositorUID] DEFAULT (suser_id()) NOT NULL,
    [PO_PositorNote]     VARCHAR (348)      NULL,
    [PO_PositorInserted] DATETIMEOFFSET (7) CONSTRAINT [DF__PO_Positor_PO_PositorInserted] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PO_RowGuid]         UNIQUEIDENTIFIER   CONSTRAINT [DF__PO_Positor_PO_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__Positor] PRIMARY KEY NONCLUSTERED ([PO_PositorId] ASC),
    CONSTRAINT [CL__PO_Positor_PO_PositorApp] CHECK ([PO_PositorApp]<>''),
    CONSTRAINT [CL__PO_Positor_PO_PositorHost] CHECK ([PO_PositorHost]<>''),
    CONSTRAINT [CL__PO_Positor_PO_PositorName] CHECK ([PO_PositorName]<>''),
    CONSTRAINT [CL__PO_Positor_PO_PositorNote] CHECK ([PO_PositorNote]<>''),
    CONSTRAINT [UK__Positor] UNIQUE CLUSTERED ([PO_PositorApp] ASC, [PO_PositorHost] ASC, [PO_PositorUID] ASC),
    CONSTRAINT [UX__PO_Positor_PO_RowGuid] UNIQUE NONCLUSTERED ([PO_RowGuid] ASC)
);

