﻿CREATE FUNCTION [posit].[Get_PositorId]
(
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @PositorId	INT = NULL;

	SELECT TOP 1
		@PositorId = [p].[PO_PositorId]
	FROM [posit].[PO_Positor] [p]
	WHERE	[p].[PO_PositorApp]		= APP_NAME()
		AND	[p].[PO_PositorHost]	= HOST_NAME()
		AND	[p].[PO_PositorUID]		= SUSER_ID();

	RETURN @PositorId;

END;