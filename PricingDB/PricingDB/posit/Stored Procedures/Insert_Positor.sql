﻿CREATE PROCEDURE [posit].[Insert_Positor]
(
	@PositorNote	NVARCHAR(348) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(261) = N'[' + OBJECT_SCHEMA_NAME(@@PROCID) + N'].[' + OBJECT_NAME(@@PROCID) + N']';
	SET @PositorNote = COALESCE(@PositorNote, N'Automatically created by: ' + @ProcedureDesc);

	BEGIN TRY

		INSERT INTO [posit].[PO_Positor]
		(
			[PO_PositorApp],
			[PO_PositorHost],
			[PO_PositorUID],
			[PO_PositorNote]
		)
		SELECT
			[t].[PO_PositorApp],
			[t].[PO_PositorHost],
			[t].[PO_PositorUID],
				[PO_PositorNote]	= @PositorNote
		FROM (VALUES
		(
			APP_NAME(),
			HOST_NAME(),
			SUSER_ID()
		)) [t] ([PO_PositorApp], [PO_PositorHost], [PO_PositorUID])
		LEFT OUTER JOIN
			[posit].[PO_Positor]	[p]
				ON	[p].[PO_PositorApp]		= [t].[PO_PositorApp]
				AND	[p].[PO_PositorHost]	= [t].[PO_PositorHost]
				AND	[p].[PO_PositorUID]		= [t].[PO_PositorUID]
		WHERE
			[p].[PO_PositorId]	IS NULL;

	END TRY
	BEGIN CATCH
	END CATCH;

	RETURN [posit].[Get_PositorId]();

END;