﻿CREATE TABLE [stage].[PP_PricingPlattsFtp_Duple] (
    [PP_ID]                   INT                IDENTITY (1, 1) NOT NULL,
    [PP_FilePath]             VARCHAR (260)      NOT NULL,
    [PP_FileCopyright]        VARCHAR (48)       NOT NULL,
    [PP_FileIdentifier]       VARCHAR (16)       NOT NULL,
    [PP_FileCreated_Stamp]    CHAR (12)          NOT NULL,
    [PP_FileCreated]          DATETIMEOFFSET (0) NOT NULL,
    [PP_FileItemCount]        INT                NOT NULL,
    [PP_FileLabel]            VARCHAR (9)        NOT NULL,
    [PP_FileCategoryCode]     VARCHAR (3)        NOT NULL,
    [PP_FileAsOf_Stamp]       CHAR (8)           NOT NULL,
    [PP_FileAsOf]             DATETIMEOFFSET (0) NOT NULL,
    [PP_ItemTrans]            VARCHAR (2)        NOT NULL,
    [PP_ItemSymbol]           CHAR (7)           NOT NULL,
    [PP_ItemBateCode]         CHAR (1)           NOT NULL,
    [PP_ItemAssessment_Stamp] CHAR (12)          NOT NULL,
    [PP_ItemAssessment]       DATETIMEOFFSET (0) NOT NULL,
    [PP_ItemPrice]            DECIMAL (14, 4)    NULL,
    [PP_ItemXAction_Stamp]    CHAR (12)          NULL,
    [PP_ItemXAction]          DATETIMEOFFSET (0) NULL,
    [PP_ChangedAt]            DATETIMEOFFSET (7) NOT NULL,
    [PP_PositedBy]            INT                CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [PP_PositedAt]            DATETIMEOFFSET (7) CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PP_PositReliability]     TINYINT            CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_Reliability] DEFAULT ((50)) NOT NULL,
    [PP_PositReliable]        AS                 (CONVERT([bit],case when [PP_PositReliability]>(0) then (1) else (0) end)) PERSISTED NOT NULL,
    [PP_RowGuid]              UNIQUEIDENTIFIER   CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__PP_PricingPlattsFtp_Duple] PRIMARY KEY CLUSTERED ([PP_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileAsOf_Stamp] CHECK ([PP_FileAsOf_Stamp]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileCategoryCode] CHECK ([PP_FileCategoryCode]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileCopyright] CHECK ([PP_FileCopyright]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileCreated_Stamp] CHECK ([PP_FileCreated_Stamp]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileIdentifier] CHECK ([PP_FileIdentifier]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileItemCount] CHECK ([PP_FileItemCount]>(0)),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileLabel] CHECK ([PP_FileLabel]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FilePath] CHECK ([PP_FilePath]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemAssessment_Stamp] CHECK ([PP_ItemAssessment_Stamp]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemBateCode] CHECK ([PP_ItemBateCode]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemSymbol] CHECK ([PP_ItemSymbol]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemTrans] CHECK ([PP_ItemTrans]<>''),
    CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemXAction_Stamp] CHECK ([PP_ItemXAction_Stamp]<>''),
    CONSTRAINT [FK__PP_PricingPlattsFtp_Duple_PP_PositedBy] FOREIGN KEY ([PP_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UX__PP_PricingPlattsFtp_Duple_PP_RowGuid] UNIQUE NONCLUSTERED ([PP_RowGuid] ASC)
);

