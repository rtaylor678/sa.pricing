﻿CREATE VIEW [stage].[PP_PricingPlattsFtp_Posit_Recent]
WITH SCHEMABINDING
AS SELECT
	[p].[PP_ItemSymbol],
	[p].[PP_ItemBateCode],
	[p].[PP_ItemAssessment],
	[p].[PP_ChangedAt],
	[p].[PP_PositedAt]
FROM
	[stage].[PP_PricingPlattsFtp_Posit]	[p]
WHERE
	[p].[PP_PositedAt] >= CONVERT(DATE, '20150101', 112);
GO
CREATE UNIQUE CLUSTERED INDEX [PK__PP_PricingPlattsFtp_Posit_Recent]
    ON [stage].[PP_PricingPlattsFtp_Posit_Recent]([PP_ItemSymbol] ASC, [PP_ItemBateCode] ASC, [PP_ItemAssessment] ASC, [PP_ChangedAt] DESC, [PP_PositedAt] DESC) WITH (FILLFACTOR = 95, STATISTICS_NORECOMPUTE = ON);

