﻿CREATE VIEW [stage].[PP_PricingPlattsFtp_Posit_Pivot]
WITH SCHEMABINDING
AS
SELECT
	[u].[PP_FilePath],
	[u].[PP_FileCopyright],
	[u].[PP_FileIdentifier],
	[u].[PP_FileCreated_Stamp],
	[u].[PP_FileCreated],

	[u].[PP_FileItemCount],
	[u].[PP_FileLabel],
	[u].[PP_FileCategoryCode],
	[u].[PP_FileAsOf_Stamp],
	[u].[PP_FileAsOf],

	[u].[PP_ItemTrans],
	[u].[PP_ItemSymbol],
	[u].[PP_ItemAssessment_Stamp],
	[u].[PP_ItemAssessment],

	[u].[PP_ItemXAction_Stamp],
	[u].[PP_ItemXAction],

	[u].[PP_ChangedAt],
	[u].[PP_PositedBy],
	[u].[PP_PositedAt],
	[u].[PP_PositReliability],
	[u].[PP_PositReliable],

		[PP_Open]			= [u].[o],
		[PP_Close]			= [u].[c],
		[PP_Low]			= [u].[l],
		[PP_High]			= [u].[h],
		[PP_Volume]			= [u].[w],

		[PP_Index]			= [u].[u],
		[PP_Mean]			= [u].[m],
		[PP_Ask]			= [u].[a],
		[PP_Bid]			= [u].[b],
		[PP_PrevInterest]	= [u].[e]

FROM (
	SELECT
		[p].[PP_FilePath],
		[p].[PP_FileCopyright],
		[p].[PP_FileIdentifier],
		[p].[PP_FileCreated_Stamp],
		[p].[PP_FileCreated],

		[p].[PP_FileItemCount],
		[p].[PP_FileLabel],
		[p].[PP_FileCategoryCode],
		[p].[PP_FileAsOf_Stamp],
		[p].[PP_FileAsOf],

		[p].[PP_ItemTrans],
		[p].[PP_ItemSymbol],
		[p].[PP_ItemBateCode],
		[p].[PP_ItemAssessment_Stamp],
		[p].[PP_ItemAssessment],

		[p].[PP_ItemPrice],

		[p].[PP_ItemXAction_Stamp],
		[p].[PP_ItemXAction],

		[p].[PP_ChangedAt],
		[p].[PP_PositedBy],
		[p].[PP_PositedAt],
		[p].[PP_PositReliability],
		[p].[PP_PositReliable]
	FROM
		[stage].[PP_PricingPlattsFtp_Posit]	[p]
	) [p]
	PIVOT
	(
		MAX([p].[PP_ItemPrice]) FOR [p].[PP_ItemBateCode] IN ([a], [b], [c], [e], [h], [l], [m], [o], [u], [w])
	) [u];