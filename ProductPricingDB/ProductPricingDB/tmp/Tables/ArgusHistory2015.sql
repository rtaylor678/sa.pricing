﻿CREATE TABLE [tmp].[ArgusHistory2015] (
    [LineNum]      BIGINT       NULL,
    [Symbol]       VARCHAR (9)  NULL,
    [TimeStampID]  TINYINT      NULL,
    [PriceTypeID]  TINYINT      NULL,
    [PriceDate]    DATE         NULL,
    [PriceValue]   MONEY        NULL,
    [FwdPeriod]    TINYINT      NULL,
    [DiffBaseRoll] TINYINT      NULL,
    [PriceYear]    SMALLINT     NULL,
    [ContFwd]      TINYINT      NULL,
    [RecordStatus] VARCHAR (1)  NULL,
    [Filename]     VARCHAR (29) NULL,
    [FileDate]     DATE         NULL,
    [RecCreated]   DATETIME     NOT NULL,
    [Processed]    BIT          NULL
);

