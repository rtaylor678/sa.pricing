﻿CREATE TABLE [tmp].[DeletedArgusPrices] (
    [PriceID]      INT          IDENTITY (1, 1) NOT NULL,
    [Source]       VARCHAR (10) NOT NULL,
    [FtpFileID]    INT          NULL,
    [TransType]    CHAR (1)     NULL,
    [ItemCode]     VARCHAR (10) NOT NULL,
    [PriceTypeID]  VARCHAR (2)  NULL,
    [ProductID]    VARCHAR (12) NOT NULL,
    [PriceDate]    DATE         NULL,
    [PriceValue]   MONEY        NULL,
    [TimeStampID]  TINYINT      NULL,
    [FwdPeriod]    SMALLINT     NULL,
    [DiffBaseRoll] SMALLINT     NULL,
    [ContFwd]      TINYINT      NULL,
    [PriceYear]    INT          NULL,
    [PriceMonth]   INT          NULL,
    [RecCreated]   DATETIME     NOT NULL
);

