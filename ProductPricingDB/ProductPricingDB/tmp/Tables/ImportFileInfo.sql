﻿CREATE TABLE [tmp].[ImportFileInfo] (
    [RowID]   INT           IDENTITY (1, 1) NOT NULL,
    [RowText] VARCHAR (MAX) NULL
);

