﻿CREATE TABLE [tmp].[ArgusLatestDoc] (
    [Id]                BIGINT       NOT NULL,
    [Module]            VARCHAR (16) NOT NULL,
    [ModuleDesc]        VARCHAR (51) NOT NULL,
    [Folder]            VARCHAR (25) NOT NULL,
    [FileName]          VARCHAR (25) NOT NULL,
    [Category]          VARCHAR (70) NOT NULL,
    [Code]              VARCHAR (9)  NOT NULL,
    [DisplayName]       VARCHAR (95) NOT NULL,
    [ContFwdPeriod]     VARCHAR (3)  NOT NULL,
    [TimeStampID]       VARCHAR (3)  NOT NULL,
    [TimeStamp]         VARCHAR (20) NOT NULL,
    [PriceTypeID]       SMALLINT     NOT NULL,
    [PriceType]         VARCHAR (17) NOT NULL,
    [DeliveryMode]      VARCHAR (15) NOT NULL,
    [Timing]            VARCHAR (15) NOT NULL,
    [FwdPeriodDesc]     VARCHAR (90) NOT NULL,
    [DiffBasis]         VARCHAR (50) NOT NULL,
    [DiffBasisTiming]   VARCHAR (5)  NULL,
    [StartDate]         DATE         NULL,
    [EndDate]           DATE         NULL,
    [OldCode]           VARCHAR (9)  NULL,
    [Unit]              VARCHAR (15) NOT NULL,
    [Frequency]         VARCHAR (11) NOT NULL,
    [StartDateInModule] DATE         NULL,
    [EndDateInModule]   DATE         NULL,
    [DecimalPlaces]     TINYINT      NOT NULL,
    CONSTRAINT [PK_ArgusLatestDoc] PRIMARY KEY CLUSTERED ([Id] ASC)
);

