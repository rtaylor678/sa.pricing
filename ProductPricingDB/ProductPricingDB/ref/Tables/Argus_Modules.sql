﻿CREATE TABLE [ref].[Argus_Modules] (
    [Module]        VARCHAR (25) NOT NULL,
    [Path]          VARCHAR (30) NOT NULL,
    [FileName]      VARCHAR (30) NOT NULL,
    [Description]   VARCHAR (64) NOT NULL,
    [Folder]        VARCHAR (30) NOT NULL,
    [Time]          CHAR (12)    NOT NULL,
    [LocalTime]     TIME (7)     NOT NULL,
    [LocalTimeZone] VARCHAR (24) NOT NULL
);

