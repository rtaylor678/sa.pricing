﻿CREATE TABLE [ref].[Argus_Codes] (
    [ItemCode]     CHAR (10)     NOT NULL,
    [DisplayName]  VARCHAR (192) NOT NULL,
    [DeliveryMode] VARCHAR (12)  NOT NULL,
    [Unit]         VARCHAR (16)  NOT NULL,
    [Frequency]    VARCHAR (12)  NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [ItemCode]
    ON [ref].[Argus_Codes]([ItemCode] ASC);

