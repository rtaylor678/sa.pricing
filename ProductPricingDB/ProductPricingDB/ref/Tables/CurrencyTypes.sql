﻿CREATE TABLE [ref].[CurrencyTypes] (
    [CurId]                    INT           IDENTITY (1, 1) NOT NULL,
    [CurTag]                   VARCHAR (4)   NOT NULL,
    [CurName]                  NVARCHAR (48) NOT NULL,
    [CurDetail]                NVARCHAR (48) NOT NULL,
    [CurGlyph]                 NCHAR (1)     NULL,
    [CurSubUnit]               VARCHAR (48)  NULL,
    [CurSubUnitGlyph]          NCHAR (1)     NULL,
    [CurMultiplierSubUnit]     FLOAT (53)    NULL,
    [CurMultiplierStored]      FLOAT (53)    NULL,
    [CurMultiplierReported]    FLOAT (53)    NULL,
    [CurLastUsed_Year]         SMALLINT      NULL,
    [CurReplacementCurrencyID] INT           NULL,
    [CurISONumeric]            VARCHAR (4)   NULL,
    CONSTRAINT [PK_CurrencyTypes] PRIMARY KEY CLUSTERED ([CurId] ASC)
);

