﻿CREATE TABLE [ref].[Argus_PriceType] (
    [PriceTypeId] TINYINT      NOT NULL,
    [Description] VARCHAR (26) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [PriceTypeId]
    ON [ref].[Argus_PriceType]([PriceTypeId] ASC);

