﻿CREATE TABLE [ref].[Argus_TimeStamp] (
    [TimeStampId] TINYINT      NOT NULL,
    [Description] VARCHAR (26) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [TimeStampId]
    ON [ref].[Argus_TimeStamp]([TimeStampId] ASC);

