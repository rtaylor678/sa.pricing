﻿CREATE TABLE [ref].[FrequencyTypes] (
    [FreqyId]    INT           IDENTITY (1, 1) NOT NULL,
    [Source]     VARCHAR (10)  NULL,
    [FreqTag]    CHAR (2)      NOT NULL,
    [FreqName]   NVARCHAR (14) NOT NULL,
    [FreqDetail] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_FrequencyTypes] PRIMARY KEY CLUSTERED ([FreqyId] ASC)
);

