﻿CREATE TABLE [ref].[ModuleTypes] (
    [AR_ID]            INT          IDENTITY (1, 1) NOT NULL,
    [Source]           VARCHAR (10) NULL,
    [AR_Module]        VARCHAR (16) NOT NULL,
    [AR_Path]          VARCHAR (24) NOT NULL,
    [AR_FileName]      VARCHAR (16) NOT NULL,
    [AR_Description]   VARCHAR (64) NOT NULL,
    [AR_Folder]        VARCHAR (24) NOT NULL,
    [AR_Time]          CHAR (12)    NOT NULL,
    [AR_LocalTime]     TIME (7)     NOT NULL,
    [AR_LocalTimeZone] VARCHAR (24) NOT NULL
);

