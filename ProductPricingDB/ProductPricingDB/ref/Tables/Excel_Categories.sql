﻿CREATE TABLE [ref].[Excel_Categories] (
    [CatKey]         VARCHAR (3)    NOT NULL,
    [CatCommodity]   VARCHAR (128)  NOT NULL,
    [CatCode]        VARCHAR (50)   NULL,
    [CatDisplayName] VARCHAR (128)  NULL,
    [CatCategory]    NVARCHAR (128) NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([CatKey] ASC)
);

