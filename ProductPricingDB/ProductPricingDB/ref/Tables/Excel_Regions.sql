﻿CREATE TABLE [ref].[Excel_Regions] (
    [RegKey]  VARCHAR (3)   NOT NULL,
    [RegName] VARCHAR (128) NOT NULL,
    CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED ([RegKey] ASC)
);

