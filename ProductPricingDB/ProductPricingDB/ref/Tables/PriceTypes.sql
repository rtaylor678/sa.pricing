﻿CREATE TABLE [ref].[PriceTypes] (
    [AR_ID]          INT          IDENTITY (1, 1) NOT NULL,
    [Source]         VARCHAR (10) NULL,
    [AR_PriceTypeId] TINYINT      NOT NULL,
    [AR_Description] VARCHAR (26) NOT NULL
);

