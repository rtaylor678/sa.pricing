﻿CREATE TABLE [ref].[UOMTypes] (
    [UomId]     INT           IDENTITY (1, 1) NOT NULL,
    [Source]    VARCHAR (10)  NULL,
    [UomTag]    VARCHAR (12)  NOT NULL,
    [UomName]   NVARCHAR (24) NOT NULL,
    [UomDetail] NVARCHAR (24) NOT NULL,
    [UomGlyph]  NVARCHAR (5)  NULL,
    [UomFactor] FLOAT (53)    NULL,
    CONSTRAINT [PK_UOMTypes] PRIMARY KEY CLUSTERED ([UomId] ASC)
);

