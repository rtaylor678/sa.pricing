﻿CREATE TABLE [ref].[Watchlists] (
    [Source]   VARCHAR (6)  NOT NULL,
    [ItemCode] VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_Watchlists] PRIMARY KEY CLUSTERED ([ItemCode] ASC)
);

