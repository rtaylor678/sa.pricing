﻿CREATE TABLE [ref].[Excel_Locations] (
    [LocID]   INT           IDENTITY (1, 1) NOT NULL,
    [LocName] VARCHAR (128) NOT NULL,
    [RegKey]  VARCHAR (3)   NULL,
    CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED ([LocID] ASC)
);

