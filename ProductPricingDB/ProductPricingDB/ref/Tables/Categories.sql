﻿CREATE TABLE [ref].[Categories] (
    [Source]        VARCHAR (6)   CONSTRAINT [DF_DataFilesTypes_Source] DEFAULT ('Platts') NOT NULL,
    [Module]        VARCHAR (20)  NOT NULL,
    [Category]      VARCHAR (50)  NOT NULL,
    [CategoryGroup] VARCHAR (128) NULL,
    [Path]          VARCHAR (255) NULL,
    CONSTRAINT [PK_DataFilesTypes] PRIMARY KEY CLUSTERED ([Source] ASC, [Module] ASC)
);

