﻿CREATE TABLE [ref].[Argus_Category] (
    [ItemCode]    VARCHAR (10)   NOT NULL,
    [DisplayName] VARCHAR (192)  NOT NULL,
    [Category]    NVARCHAR (128) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [ItemCode]
    ON [ref].[Argus_Category]([ItemCode] ASC);

