﻿CREATE ROLE [app_owner]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [app_owner] ADD MEMBER [app];

