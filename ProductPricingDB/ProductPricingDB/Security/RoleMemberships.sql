﻿ALTER ROLE [db_owner] ADD MEMBER [ejb];


GO
ALTER ROLE [db_owner] ADD MEMBER [rdt];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [rdt];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [rdt];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [rdt];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [rdt];


GO
ALTER ROLE [db_datareader] ADD MEMBER [SOLOMONWORLD\DB_APP_ProductPricing];


GO
ALTER ROLE [db_datareader] ADD MEMBER [PricingUser];


GO
ALTER ROLE [db_datareader] ADD MEMBER [DBB];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [PricingUser];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [rdt];

