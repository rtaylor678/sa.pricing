﻿

CREATE PROCEDURE [dbo].[Platts_Prices]
AS
BEGIN


	DECLARE @cols AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @query AS NVARCHAR(MAX);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	SELECT @cols = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		   @cols2 = STUFF(( SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')


	SET @query = '
		SELECT Source = ''Platts'', ItemCode = ParentCode, PriceTypeID = ISNULL(ISNULL(batesc.Bates, bateshl.Bates),''c''), ParentCode = NULL, PCOrder = NULL, CNUM, MNUM, ExcelKeyCode, ExcelKey = CatCommodity, Module = ac.MDC, Category = ISNULL(pc.Category,'''') + CASE WHEN pc.CategoryGroup IS NULL THEN '' '' ELSE '' -> '' + pc.CategoryGroup END,
			Region, Location, Description, Curr, ac.UOM, Frequency = Freq, ' + @Cols2 + ', Reason FROM (
					SELECT ParentCode, ' + @cols2 + '
					FROM (  
							select ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue from (
										select p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
											MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY  PriceYear, PriceMonth, ParentCodeOrder)
										from Platts_ProductPricesByMonthConverted p
											left join solomon.ParentList pl on pl.ItemCode = p.ItemCode and pl.PriceTypeID =  CAST(p.PriceTypeId AS VARCHAR(3))

							) a
							where ParentCodeOrder = minpriority 
		
							) T
					PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c
		LEFT JOIN (SELECT Symbol, Curr, Freq, UOM, Description, MDC FROM (SELECT Symbol, Curr, Freq, UOM, Description, MDC, row_number() over (partition by Symbol order by MDC) as rn FROM ref.Platts_Symbols) t WHERE rn = 1) ac on ac.Symbol = c.ParentCode
		--note on above; Symbol table can have duplicates with just the MDC being different, we take the lowest of them sorted
		LEFT JOIN solomon.RegionLocation rl on rl.ItemCode = c.ParentCode
		LEFT JOIN solomon.CandMNums cm on cm.ItemCode = c.ParentCode
		LEFT JOIN solomon.ExcelKeys ek ON ek.ItemCode = c.ParentCode
		LEFT JOIN ref.Excel_Categories ec ON ec.CatKey = ek.ExcelKeyCode
		LEFT JOIN ref.Platts_Categories pc ON pc.Module = ac.MDC
		LEFT JOIN (SELECT DISTINCT Symbol, Bates = ''c'' FROM ref.Platts_Symbols WHERE Bates LIKE ''%c%'' OR Bates LIKE ''%u%'') batesc on batesc.Symbol = c.ParentCode
		LEFT JOIN (SELECT DISTINCT Symbol, Bates= ''h'' FROM ref.Platts_Symbols WHERE Bates LIKE ''%h%'') bateshl on bateshl.Symbol = c.ParentCode
		LEFT JOIN solomon.UOMConversions conv ON conv.ItemCode = c.ParentCode
		WHERE c.ParentCode NOT IN (SELECT ItemCode FROM solomon.NotNeeded)
		ORDER BY ParentCode
	'
	

	EXEC(@Query)








END



