﻿


CREATE PROCEDURE [dbo].[OLDAll_Prices_All_Data_Rolled_Up]
AS
BEGIN

	DECLARE @cols AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @query AS NVARCHAR(MAX), @cols3 as nvarchar(max);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	SELECT @cols = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		   @cols2 = STUFF(( SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols3 = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) + ' real' 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')


	set @query = 'create table #tmp (Source varchar(6), ItemCode char(9), PriceTypeId varchar(10), ParentCode varchar(10), ParentCodeOrder int, CNUM varchar(50), 
		CNUM2 varchar(50), MNUM varchar(50), MNUM2 varchar(50), CatCommodity varchar(128), Module varchar(16), Category nvarchar(260), Region nvarchar(128), Location nvarchar(255), 
		DisplayName varchar(192), Curr varchar(16), UOM varchar(16), Frequency varchar(12), ' + @cols3 + '); 
		INSERT #tmp EXECUTE All_Prices_All_Data; 
		SELECT * FROM #tmp'

  print @query

	EXEC(@Query)

	


END




