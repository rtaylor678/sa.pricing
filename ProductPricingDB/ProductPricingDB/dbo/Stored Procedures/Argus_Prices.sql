﻿
CREATE PROCEDURE [dbo].[Argus_Prices]
AS
BEGIN

	DECLARE @cols AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @query AS NVARCHAR(MAX);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	SELECT @cols = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		   @cols2 = STUFF(( SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')


	SET @query = '
		SELECT Source = ''Argus'', ItemCode = ParentCode, PriceTypeID = 8, ParentCode = NULL, PCOrder = NULL, CNUM, MNUM, ExcelKeyCode, ExcelKey = CatCommodity, Module = ''DAGM'', Category,
			Region, Location, ac.DisplayName, Curr = CASE WHEN CHARINDEX(''/'', Unit) = 0 THEN Unit ELSE LEFT(Unit,CHARINDEX(''/'', Unit)-1) END, 
			UOM = CASE WHEN CHARINDEX(''/'', Unit) = 0 THEN '''' ELSE RIGHT(Unit, LEN(RTRIM(Unit)) - CHARINDEX(''/'',Unit)) END,
			Frequency, ' + @Cols2 + ' FROM (
					SELECT ParentCode, ' + @cols2 + '
					FROM (  
							select ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue from (
										select p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
											MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY  PriceYear, PriceMonth, ParentCodeOrder)
										from Argus_ProductPricesByMonth p
											left join solomon.ParentList pl on pl.ItemCode = p.ItemCode and pl.PriceTypeID =  CAST(p.PriceTypeId AS VARCHAR(3))

							) a
							where ParentCodeOrder = minpriority 
		
							) T
					PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c
		LEFT JOIN ref.Argus_Codes ac on ac.ItemCode = c.ParentCode
		LEFT JOIN solomon.RegionLocation rl on rl.ItemCode = c.ParentCode
		LEFT JOIN solomon.CandMNums cm on cm.ItemCode = c.ParentCode
		LEFT JOIN (SELECT DISTINCT * FROM solomon.ExcelKeys) ek ON ek.ItemCode = c.ParentCode
		LEFT JOIN ref.Excel_Categories ec ON ec.CatKey = ek.ExcelKeyCode
		LEFT JOIN ref.Argus_Category acat ON acat.ItemCode = c.ParentCode
		ORDER BY ParentCode
	'

	EXEC(@Query)






END


