﻿


CREATE  PROCEDURE [dbo].[All_Prices]
AS
BEGIN

	DECLARE @query AS NVARCHAR(MAX),
		@cols AS NVARCHAR(MAX),
		@cols2 AS NVARCHAR(MAX),
		@cols3 AS NVARCHAR(MAX),
		@cols4 AS NVARCHAR(MAX),
		@cols5 AS NVARCHAR(MAX);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	--each of these gives a different definition of the list of columns in the data, to be used further down
	SELECT @cols = STUFF((SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols2 = STUFF((SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols3 = STUFF((SELECT DISTINCT ',' + QUOTENAME(YearMonth) + ' real' 
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols4 = STUFF((SELECT DISTINCT ', a.' + QUOTENAME(YearMonth) + ' + CASE WHEN rp.ItemCode IS NULL THEN 0 ELSE b.'  + QUOTENAME(YearMonth) + ' END AS '  + QUOTENAME(YearMonth)
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols5 = STUFF((SELECT DISTINCT ' UPDATE t SET ' + QUOTENAME(YearMonth) + ' = ' + QUOTENAME(YearMonth) + ' * cc.ConversionRate FROM #tmp2 t
		INNER JOIN CurrencyConv cc ON t.Curr = cc.FromCurrency and cc.ToCurrency = ''USD'' and cc.PriceYear = ' 
		+ LEFT(YearMonth, 4) + ' and cc.PriceMonth = ' + RIGHT(YearMonth, 2) + ';'
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'');

	SET @query = 'CREATE TABLE #tmp (Source VARCHAR(10), ItemCode VARCHAR(10), ' + @cols3 + '); 

		--grabs the data from the UOM converted views, gets the values based on a partition of the ParentList (getting the lowest priority value each time)
		--then pivots them into the tmp table for further processing
		INSERT #tmp
		SELECT Source = ''Argus'', ItemCode = ParentCode, ' + @Cols2 + ' 
		FROM (
				SELECT ParentCode, ' + @cols2 + '
				FROM (  
						SELECT ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue 
						FROM (
								SELECT p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
									MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY PriceYear, PriceMonth, ParentCodeOrder)
							--	FROM Argus_ProductPricesByMonth p
								FROM Argus_ProductPricesByMonthConverted p
									INNER JOIN solomon.ParentList pl ON pl.ItemCode = p.ItemCode AND pl.PriceTypeID = p.PriceTypeId
						) a
						WHERE ParentCodeOrder = MinPriority AND ParentCodeOrder = 1
				) T
				PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c

		--WHERE NOT EXISTS (	SELECT 1
		--					FROM  solomon.NotNeeded nn
		--					WHERE nn.ItemCode = c.ParentCode)
		WHERE c.ParentCode NOT IN (SELECT ItemCode FROM solomon.NotNeeded)
		UNION ALL
		SELECT Source = ''Platts'', ItemCode = ParentCode, ' + @Cols2 + ' 
		FROM (
				SELECT ParentCode, ' + @cols2 + '
				FROM (  
						SELECT ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue 
						FROM (
								SELECT p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
									MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY PriceYear, PriceMonth, ParentCodeOrder)
						--		FROM Platts_ProductPricesByMonth p
								FROM Platts_ProductPricesByMonthConverted p
									INNER JOIN solomon.ParentList pl ON pl.ItemCode = p.ItemCode AND pl.PriceTypeID = p.PriceTypeId
						) a
						WHERE ParentCodeOrder = MinPriority AND ParentCodeOrder = 1
		
				) T
				PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c
		WHERE c.ParentCode NOT IN (SELECT ItemCode FROM solomon.NotNeeded)
		--WHERE NOT EXISTS (	SELECT 1
		--					FROM  solomon.NotNeeded nn
		--					WHERE nn.ItemCode = c.ParentCode)

		--this next part adds on the Product Descriptions


		CREATE TABLE #tmp2 (Source VARCHAR(6), ItemCode CHAR(9), PriceTypeId VARCHAR(10), ParentCode VARCHAR(10), ParentCodeOrder INT, CNUM VARCHAR(50), 
			CNUM2 VARCHAR(50), MNUM VARCHAR(50), MNUM2 VARCHAR(50), CatCommodity VARCHAR(128), Module VARCHAR(16), Category NVARCHAR(260), Region NVARCHAR(128), Location NVARCHAR(255), 
			DisplayName VARCHAR(192), Curr VARCHAR(16), CurrNew VARCHAR(16), UOM VARCHAR(16), Reason VARCHAR(250), ConversionRate float, Frequency VARCHAR(12), ' + @cols3 + '); 

		INSERT #tmp2
		SELECT a.Source, a.ItemCode, pi.PriceTypeId, pi.ParentCode, pi.ParentCodeOrder, pi.CNUM, pi.CNUM2, pi.MNUM, pi.MNUM2, 
			pi.CatCommodity, pi.Module, pi.Category, pi.Region, pi.Location, pi.DisplayName
			, pi.Curr
			, CASE	WHEN pi.Curr = ''USC'' THEN ''USD''
					WHEN cc.ToCurrency IS NULL THEN pi.Curr 
					ELSE cc.ToCurrency END AS CurrNew 
			, CASE	WHEN uc.To_UOM IS NOT NULL THEN uc.To_UOM
					WHEN su.UOMStandard IS NULL THEN pi.UOM
					ELSE su.UOMStandard END AS UOM
			, ISNULL(uc.Reason,'''') AS Reason
			, uc.ConversionRate AS ConversionRate
			, Frequency, ' + @cols + ' 
		FROM #tmp a 
			LEFT JOIN dbo.Product_Descriptions pi ON pi.ItemCode = a.ItemCode AND ParentCodeOrder = 1
			LEFT JOIN solomon.StandardUOM su ON a.Source = su.Source AND pi.UOM = su.UOMSource
			LEFT JOIN solomon.UOMConversions uc ON pi.ItemCode = uc.ItemCode --and pi.PriceTypeID = uc.PriceTypeID
		    LEFT JOIN solomon.CurrencyConversions cc ON pi.Curr = cc.FromCurrency AND cc.ToCurrency = ''USD''



		' + @cols5 + '



				--then we select the output, joining to ReferencePrices to add the price to a parent if it is a Diff price

		CREATE INDEX tmp ON #tmp2(ItemCode);

		SELECT a.Source, a.ItemCode, a.PriceTypeId, a.ParentCode, a.ParentCodeOrder, a.CNUM, a.CNUM2, a.MNUM, a.MNUM2, 
			a.CatCommodity, a.Module, a.Category, a.Region, a.Location, a.DisplayName, a.CurrNew AS Curr, a.UOM, a.Reason, a.ConversionRate, a.Frequency, ' + @cols4 + ' 
		FROM #tmp2 a 
			LEFT JOIN solomon.ReferencePrices rp ON rp.ItemCode = a.ItemCode LEFT JOIN #tmp2 b ON b.ItemCode = rp.refcode
			--LEFT JOIN solomon.UOMConversions uomc ON uomc.ItemCode = a.ItemCode AND uomc.UOM = a.UOM;
			ORDER BY a.Source,a.ItemCode;
			
			'



	--print @query


	--select @query
	EXEC(@Query)
	


END







