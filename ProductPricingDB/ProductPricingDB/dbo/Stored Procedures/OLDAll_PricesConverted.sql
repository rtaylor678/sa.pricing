﻿
CREATE PROCEDURE [dbo].[OLDAll_PricesConverted]
AS
BEGIN

	DECLARE @cols AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @query AS NVARCHAR(MAX);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	SELECT @cols = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		   @cols2 = STUFF(( SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')


	SET @query = '
		SELECT Source = ''Argus'', ItemCode = ParentCode, 
			 ' + @Cols2 + ' FROM (
					SELECT ParentCode, ' + @cols2 + '
					FROM (  
							select ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue from (
										select p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
											MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY  PriceYear, PriceMonth, ParentCodeOrder)
										from Argus_ProductPricesByMonthConverted p
											left join solomon.ParentList pl on pl.ItemCode = p.ItemCode and pl.PriceTypeID =  CAST(p.PriceTypeId AS VARCHAR(3))

							) a
							where ParentCodeOrder = minpriority and parentcodeorder = 1
		
							) T
					PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c
		--LEFT JOIN ref.Argus_Codes ac on ac.ItemCode = c.ParentCode
		--LEFT JOIN solomon.RegionLocation rl on rl.ItemCode = c.ParentCode
		--LEFT JOIN solomon.CandMNums cm on cm.ItemCode = c.ParentCode
		--LEFT JOIN (SELECT DISTINCT * FROM solomon.ExcelKeys) ek ON ek.ItemCode = c.ParentCode
		--LEFT JOIN ref.Excel_Categories ec ON ec.CatKey = ek.ExcelKeyCode
		--LEFT JOIN ref.Argus_Category acat ON acat.ItemCode = c.ParentCode
		--ORDER BY ParentCode

		UNION ALL

		SELECT Source = ''Platts'', ItemCode = ParentCode, 
			' + @Cols2 + ' FROM (
					SELECT ParentCode, ' + @cols2 + '
					FROM (  
							select ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue from (
										select p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
											MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY  PriceYear, PriceMonth, ParentCodeOrder)
										from Platts_ProductPricesByMonthConverted p
											left join solomon.ParentList pl on pl.ItemCode = p.ItemCode and pl.PriceTypeID =  CAST(p.PriceTypeId AS VARCHAR(3))

							) a
							where ParentCodeOrder = minpriority and parentcodeorder = 1
		
							) T
					PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c
		--LEFT JOIN (SELECT Symbol, Curr, Freq, UOM, Description, MDC FROM (SELECT Symbol, Curr, Freq, UOM, Description, MDC, row_number() over (partition by Symbol order by MDC) as rn FROM ref.Platts_Symbols) t WHERE rn = 1) ac on ac.Symbol = c.ParentCode
		----note on above; Symbol table can have duplicates with just the MDC being different, we take the lowest of them sorted
		--LEFT JOIN solomon.RegionLocation rl on rl.ItemCode = c.ParentCode
		--LEFT JOIN solomon.CandMNums cm on cm.ItemCode = c.ParentCode
		--LEFT JOIN solomon.ExcelKeys ek ON ek.ItemCode = c.ParentCode
		--LEFT JOIN ref.Excel_Categories ec ON ec.CatKey = ek.ExcelKeyCode
		--LEFT JOIN ref.Platts_Categories pc ON pc.Module = ac.MDC
		--LEFT JOIN (SELECT DISTINCT Symbol, Bates = ''c'' FROM ref.Platts_Symbols WHERE Bates LIKE ''%c%'' OR Bates LIKE ''%u%'') batesc on batesc.Symbol = c.ParentCode
		--LEFT JOIN (SELECT DISTINCT Symbol, Bates= ''h'' FROM ref.Platts_Symbols WHERE Bates LIKE ''%h%'') bateshl on bateshl.Symbol = c.ParentCode
		--LEFT JOIN solomon.Conversions conv ON conv.ItemCode = c.ParentCode
		WHERE c.ParentCode NOT IN (SELECT ItemCode FROM solomon.NotNeeded)
		--ORDER BY ParentCode
	'
	


	print @query




	EXEC(@Query)






END


