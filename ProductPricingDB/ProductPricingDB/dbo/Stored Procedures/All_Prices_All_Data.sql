﻿


CREATE PROCEDURE [dbo].[All_Prices_All_Data]
AS
BEGIN

	DECLARE @cols AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @query AS NVARCHAR(MAX), @cols3 as nvarchar(max), @cols4 as nvarchar(max);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	SELECT @cols = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		   @cols2 = STUFF(( SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols3 = STUFF((  SELECT DISTINCT ',' + QUOTENAME(YearMonth) + ' real' 
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
			@cols4 = STUFF((  SELECT DISTINCT ', case when rp.itemcode is null then a.' + QUOTENAME(YearMonth) + '  else b.'  + QUOTENAME(YearMonth) + ' + a.'  + QUOTENAME(YearMonth) + ' end as '  + QUOTENAME(YearMonth)
							FROM CTE 
							FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')

--print @cols
--print @cols2
--print @cols3
--print @cols4

	set @query = 'create table #tmp (Source varchar(10), ItemCode varchar(10), ' + @cols3 + '); 
		INSERT #tmp EXECUTE All_PricesConverted; 

		create table #tmp2 (Source varchar(6), ItemCode char(9), PriceTypeId varchar(10), ParentCode varchar(10), ParentCodeOrder int, CNUM varchar(50), 
		CNUM2 varchar(50), MNUM varchar(50), MNUM2 varchar(50), CatCommodity varchar(128), Module varchar(16), Category nvarchar(260), Region nvarchar(128), Location nvarchar(255), 
		DisplayName varchar(192), Curr varchar(16), UOM varchar(16), Frequency varchar(12), ' + @cols3 + '); 

		INSERT #tmp2
		SELECT a.Source, a.ItemCode, PriceTypeId, ParentCode, ParentCodeOrder, CNUM, CNUM2, MNUM, MNUM2, 
			CatCommodity, Module, Category, Region, Location, DisplayName, Curr, UOM, Frequency, ' + @cols + ' 
		FROM #tmp a LEFT JOIN (SELECT * FROM [ProductPricing].[dbo].[Product_Descriptions] where ParentCodeOrder = 1) pi ON pi.ItemCode = a.ItemCode;

SELECT a.Source, a.ItemCode, a.PriceTypeId, a.ParentCode, a.ParentCodeOrder, a.CNUM, a.CNUM2, a.MNUM, a.MNUM2, 
			a.CatCommodity, a.Module, a.Category, a.Region, a.Location, a.DisplayName, a.Curr, a.UOM, a.Frequency, ' + @cols4 + ' from #tmp2 a left join solomon.referenceprices rp on rp.itemcode = a.itemcode left join #tmp2 b on b.itemcode = rp.refcode;'


  --print @query

	EXEC(@Query)

	





END
