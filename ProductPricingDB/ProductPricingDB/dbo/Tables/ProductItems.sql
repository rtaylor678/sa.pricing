﻿CREATE TABLE [dbo].[ProductItems] (
    [ItemCode]        VARCHAR (10)       NOT NULL,
    [PriceTypeID]     VARCHAR (2)        CONSTRAINT [DF_Product_Items_PriceTypeID] DEFAULT ('x') NOT NULL,
    [ProductID]       AS                 (concat(ltrim(rtrim([ItemCode])),ltrim(rtrim([PriceTypeID])))) PERSISTED NOT NULL,
    [Source]          VARCHAR (6)        NULL,
    [ParentCode]      VARCHAR (10)       NULL,
    [PriorityRank]    INT                CONSTRAINT [DF_ProductItems_PriorityRank] DEFAULT ((0)) NOT NULL,
    [CNUM]            CHAR (5)           NULL,
    [MNUM]            CHAR (5)           NULL,
    [ExcelKeyCode]    VARCHAR (2)        NULL,
    [Module]          VARCHAR (16)       NULL,
    [Category]        NVARCHAR (128)     NOT NULL,
    [Region]          NVARCHAR (128)     NULL,
    [Location]        NVARCHAR (255)     NULL,
    [Frequency]       VARCHAR (12)       NULL,
    [Currency]        VARCHAR (16)       NULL,
    [UOM]             VARCHAR (16)       NULL,
    [DeliveryMode]    VARCHAR (12)       NULL,
    [Detail]          NVARCHAR (192)     NULL,
    [DisplayName]     NVARCHAR (192)     NULL,
    [Timing]          VARCHAR (12)       NULL,
    [ContFwdPeriod]   INT                NULL,
    [FwdPeriodDesc]   VARCHAR (128)      NULL,
    [TimeStampID]     INT                NULL,
    [DiffBasis]       VARCHAR (56)       NULL,
    [DiffBasisTiming] VARCHAR (5)        NULL,
    [OldCode]         VARCHAR (9)        NULL,
    [DecimalPlaces]   INT                NULL,
    [IsPrimary]       BIT                CONSTRAINT [DF_Product_Items_IsPrimary] DEFAULT ((0)) NOT NULL,
    [IsSecondary]     BIT                CONSTRAINT [DF_Product_Items_IsSecondary] DEFAULT ((0)) NOT NULL,
    [IsTertiary]      BIT                CONSTRAINT [DF_Product_Items_IsTertiary] DEFAULT ((0)) NOT NULL,
    [CSort1]          NVARCHAR (20)      NULL,
    [CSort2]          NVARCHAR (20)      NULL,
    [NotNeeded]       BIT                CONSTRAINT [DF_Product_Items_IsIgnored] DEFAULT ((0)) NOT NULL,
    [Reason]          VARCHAR (100)      NULL,
    [ExcelReviewed]   BIT                CONSTRAINT [DF_ProductItems_ExcelReviewd] DEFAULT ((0)) NOT NULL,
    [Note]            NVARCHAR (1000)    NULL,
    [Earliest_DT]     DATE               NULL,
    [Latest_DT]       DATE               NULL,
    [RecCreated]      DATETIMEOFFSET (7) CONSTRAINT [DF_Product_Items_RecCreated] DEFAULT (sysdatetimeoffset()) NULL,
    CONSTRAINT [PK_Product_Items_1] PRIMARY KEY CLUSTERED ([ItemCode] ASC, [PriceTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIdx_ProductItems_ProductID]
    ON [dbo].[ProductItems]([ProductID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Argus, Platts, etc.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductItems', @level2type = N'COLUMN', @level2name = N'Source';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Continuous Forward Period', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ProductItems', @level2type = N'COLUMN', @level2name = N'ContFwdPeriod';

