﻿CREATE TABLE [dbo].[Product_Descriptions] (
    [Source]          VARCHAR (6)    NOT NULL,
    [ItemCode]        CHAR (9)       NOT NULL,
    [PriceTypeId]     VARCHAR (10)   NOT NULL,
    [ParentCode]      VARCHAR (10)   NULL,
    [ParentCodeOrder] INT            NOT NULL,
    [CNUM]            VARCHAR (50)   NOT NULL,
    [CNUM2]           VARCHAR (50)   NOT NULL,
    [MNUM]            VARCHAR (50)   NOT NULL,
    [MNUM2]           VARCHAR (50)   NOT NULL,
    [CatCommodity]    VARCHAR (128)  NULL,
    [Module]          VARCHAR (25)   NULL,
    [Category]        NVARCHAR (260) NULL,
    [Region]          NVARCHAR (128) NULL,
    [Location]        NVARCHAR (255) NULL,
    [DisplayName]     VARCHAR (192)  NULL,
    [Curr]            VARCHAR (16)   NULL,
    [UOM]             VARCHAR (16)   NULL,
    [Frequency]       VARCHAR (12)   NULL,
    CONSTRAINT [PK_Product_Descriptions1] PRIMARY KEY CLUSTERED ([ItemCode] ASC, [PriceTypeId] ASC, [ParentCodeOrder] ASC)
);

