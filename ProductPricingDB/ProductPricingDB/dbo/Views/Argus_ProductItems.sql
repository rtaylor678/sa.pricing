﻿



CREATE VIEW [dbo].[Argus_ProductItems]
AS
	SELECT Source = 'Argus', 
		aq.ItemCode, 
		aq.PriceTypeId, 
		ParentCode = ISNULL(pl.ParentCode, aq.ItemCode), 
		ParentCodeOrder = ISNULL(pl.ParentCodeOrder, 1), 
		cm.CNUM,
		cm.CNUM2, 
		cm.MNUM,
		cm.MNUM2, 
		amd.Module, 
		acat.Category, 
		ac.Frequency, 
		ac.Unit, 
		ac.DisplayName
	FROM ref.Argus_Quotes aq
		LEFT JOIN ref.Argus_Codes ac ON ac.ItemCode = aq.ItemCode
		LEFT JOIN solomon.ParentList pl ON pl.ItemCode = ac.ItemCode 
			AND ((pl.PriceTypeID = CAST(aq.PriceTypeId AS VARCHAR)) OR (pl.PriceTypeID = '1,2' AND aq.PriceTypeId IN (1,2)) OR (pl.PriceTypeID = '6,7' AND aq.PriceTypeId IN (6,7)))
		LEFT JOIN solomon.CandMNums cm ON cm.ItemCode = aq.ItemCode
		LEFT JOIN ref.Argus_Category acat ON acat.ItemCode = ac.ItemCode
		LEFT JOIN ref.Argus_TimeStamp at ON at.TimeStampId = aq.TimeStampId
		LEFT JOIN ref.Argus_PriceType ap ON ap.PriceTypeId = aq.PriceTypeId
		LEFT JOIN ref.Argus_ModuleDetails amd ON amd.ItemCode = ac.ItemCode AND amd.TimeStampId = at.TimeStampId AND amd.PriceTypeId = ap.PriceTypeId AND amd.Module = 'DAGM'
		LEFT JOIN ref.Argus_Modules am ON am.Module = amd.Module
	WHERE aq.ItemCode IN (SELECT ItemCode FROM ProductPrices)
		--AND aq.ItemCode NOT IN (select ItemCode from solomon.NotNeeded)
		--AND aq.EndDate IS NULL





