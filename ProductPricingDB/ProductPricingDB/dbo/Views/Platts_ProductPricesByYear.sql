﻿








CREATE VIEW [dbo].[Platts_ProductPricesByYear]
AS

	SELECT ItemCode, PriceTypeId, PriceYear, PriceValue = AVG(PriceValue)
	FROM (
		SELECT pp.ItemCode, 
			PriceTypeId = UPPER(CASE WHEN pp.PriceTypeID = 'l' THEN 'h' ELSE pp.PriceTypeID END),
			PriceYear,
			PriceValue,
			TransTypeRank = DENSE_RANK() OVER (PARTITION BY pp.ItemCode, pp.PriceTypeId, pp.PriceDate ORDER BY pp.ItemCode, pp.PriceTypeId DESC, pp.PriceDate, TransType ASC),
			ContFwdRank = DENSE_RANK() OVER (PARTITION BY pp.ItemCode, pp.PriceTypeId, pp.PriceDate ORDER BY ContFwd ASC)
		FROM ProductPrices pp
		INNER JOIN solomon.ParentList pl
		ON pp.ItemCode = pl.ParentCode
		--AND pp.PriceTypeID = pl.PriceTypeID
		WHERE Source = 'Platts'
			--AND PriceTypeID IN (1,2,4,8)
		GROUP BY pp.ItemCode, pp.PriceTypeId, PriceYear, PriceDate, PriceValue, TransType, ContFwd
		) b
	WHERE TransTypeRank = 1 AND ContFwdRank = 1
	GROUP BY ItemCode, PriceTypeId, PriceYear







