﻿











CREATE VIEW [dbo].[Argus_ProductPricesByMonth]
AS

	--groups Argus data
	--only the listed PriceTypeIds (we don't use any of the others)
	--it converts PriceTypeId 2 to 1 and 7 to 6, just to make it easier in later queries to group them (1 and 2 are high-low, so we always want the average of them, same with 6 and 7)
	--if they populate a FwdPeriod we use that as the month instead of the date we received the data (because some of the data from Argus crosses those boundaries)
	--and if they populate the FwdPeriod we also check the year, and adjust it as necessary (depending how close we are to the start/end of the year we may move forward or back)

	SELECT b.ItemCode, b.PriceTypeId, PriceYear, PriceMonth, PriceValue = AVG(PriceValue)
	FROM (
		SELECT pp.ItemCode, 
			PriceTypeId = CONVERT(varchar(3),CASE pp.PriceTypeID WHEN 2 THEN 1 WHEN 7 THEN 6 ELSE pp.PriceTypeID END),
			PriceYear = CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceYear 
				ELSE CASE WHEN PriceMonth <= 2 AND (PriceMonth - FwdPeriod) <= -10 THEN PriceYear - 1 
				ELSE CASE WHEN PriceMonth >= 8 AND (PriceMonth - FwdPeriod) >= 7 THEN PriceYear + 1 
				ELSE PriceYear END END END,
			PriceMonth = CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceMonth ELSE FwdPeriod END,
			PriceValue,
			TransTypeRank = DENSE_RANK() OVER (PARTITION BY pp.ItemCode, pp.PriceTypeId, pp.PriceDate ORDER BY pp.ItemCode, pp.PriceTypeId DESC, pp.PriceDate, TransType ASC),
			ContFwdRank = DENSE_RANK() OVER (PARTITION BY pp.ItemCode, pp.PriceTypeId, pp.PriceDate ORDER BY ContFwd ASC)
		FROM ProductPrices pp
		--INNER JOIN solomon.ParentList pl
		--ON pp.ItemCode = pl.ParentCode
		--AND pp.PriceTypeID = pl.PriceTypeID
		WHERE Source = 'Argus'
			AND pp.PriceTypeID IN (1,2,3,4,5,6,7,8,20)
		GROUP BY pp.ItemCode, pp.PriceTypeId, 
			CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceYear 
				ELSE CASE WHEN PriceMonth <= 2 AND (PriceMonth - FwdPeriod) <= -10 THEN PriceYear - 1 
				ELSE CASE WHEN PriceMonth >= 8 AND (PriceMonth - FwdPeriod) >= 7 THEN PriceYear + 1 
				ELSE PriceYear END END END,
			CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceMonth ELSE FwdPeriod END, 
			PriceDate, PriceValue, TransType, ContFwd
		) b

	WHERE TransTypeRank = 1 AND ContFwdRank = 1



	GROUP BY b.ItemCode, b.PriceTypeId, PriceYear, PriceMonth












