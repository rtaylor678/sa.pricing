﻿











CREATE VIEW [dbo].[Platts_ProductPricesByMonth]
AS

	SELECT ItemCode, PriceTypeId, PriceYear, PriceMonth, PriceValue = AVG(PriceValue)
	FROM (
		SELECT pp.ItemCode, 
			PriceTypeId = CASE WHEN pp.PriceTypeID = 'l' THEN 'h' ELSE pp.PriceTypeID END,
			PriceYear,
			PriceMonth,
			PriceValue,
			TransTypeRank = DENSE_RANK() OVER (PARTITION BY pp.ItemCode, pp.PriceTypeId, pp.PriceDate ORDER BY pp.ItemCode, pp.PriceTypeId DESC, pp.PriceDate, pp.TransType ASC),
			ContFwdRank = DENSE_RANK() OVER (PARTITION BY pp.ItemCode, pp.PriceTypeId, pp.PriceDate ORDER BY ContFwd ASC)
		FROM ProductPrices pp
		--INNER JOIN solomon.ParentList pl
		--ON pp.ItemCode = pl.ParentCode
		--AND pp.PriceTypeID = pl.PriceTypeID
		WHERE Source = 'Platts'
			--AND PriceTypeID IN (1,2,4,8)
		GROUP BY pp.ItemCode, pp.PriceTypeId, PriceYear, PriceMonth, PriceDate, PriceValue, TransType, ContFwd
		) b
	WHERE TransTypeRank = 1 AND ContFwdRank = 1
	GROUP BY ItemCode, PriceTypeId, PriceYear, PriceMonth










