﻿



CREATE VIEW [dbo].[Argus_CodeDetails]
AS
	SELECT code.ItemCode, 
		code.DisplayName, 
		code.DeliveryMode, 
		code.Unit, 
		code.Frequency, 
		cat.Category, 
		quote.ContForwardPeriod AS QuotesContForwardPeriod, 
		quote.Timing, 
		quote.FwdPeriodDescription, 
		quote.TimeStampId, 
		quote.PriceTypeId, 
		quote.DifferentialBasis, 
		quote.DifferentialBasisTiming,
		quote.StartDate, 
		quote.EndDate, 
		quote.DecimalPlaces, 
		stamp.Description AS TimeStampDescription, 
		price.Description AS PriceTypeDescription, 
		detail.Module, 
		detail.ContForwardPeriod AS ModuleDetailContForwardPeriod, 
		detail.StartDate AS ModuleDetailStartDate, 
		detail.EndDate AS ModuleDetailEndDate, 
		module.Path, 
		module.FileName, 
		module.Description, 
		module.Folder, 
		module.Time, 
		module.LocalTime, 
		module.LocalTimeZone
	FROM ref.Argus_Codes code
		LEFT JOIN ref.Argus_Category cat ON cat.ItemCode = code.ItemCode
		LEFT JOIN ref.Argus_Quotes quote ON quote.ItemCode = code.ItemCode
		LEFT JOIN ref.Argus_TimeStamp stamp ON stamp.TimeStampId = quote.TimeStampId
		LEFT JOIN ref.Argus_PriceType price ON price.PriceTypeId = quote.PriceTypeId
		LEFT JOIN ref.Argus_ModuleDetails detail ON detail.ItemCode = code.ItemCode AND detail.TimeStampId = stamp.TimeStampId AND detail.PriceTypeId = quote.PriceTypeId
		LEFT JOIN ref.Argus_Modules module ON module.Module = detail.Module


