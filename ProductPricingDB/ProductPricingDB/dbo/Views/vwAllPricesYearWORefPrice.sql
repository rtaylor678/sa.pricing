﻿






CREATE VIEW [dbo].[vwAllPricesYearWORefPrice]  AS

				SELECT * FROM (

				SELECT	ParentCode
						, Source
						, ItemCode
						, CONVERT(varchar(3),PriceTypeId) AS PriceTypeID
						, ParentCodeOrder
						, CNUM
						, CNUM2
						, MNUM
						, MNUM2
						, CatCommodity
						, Module
						, Category
						, Region
						, Location
						, DisplayName
						, Curr
						, UOM
						, Frequency
						, ISNULL([2015],0) AS [2015]
						, ISNULL([2016],0) AS [2016]
						, ISNULL([2017],0) AS [2017]
						, ISNULL([2018],0) AS [2018]
						, ISNULL([2019],0) AS [2019]
						, ISNULL([2020],0) AS [2020]
						, ISNULL([2021],0) AS [2021]
						, ISNULL([2022],0) AS [2022]
						, ISNULL([2023],0) AS [2023]
						, ISNULL([2024],0) AS [2024]
						, ISNULL([2025],0) AS [2025]
				FROM (  
						SELECT ParentCode
						, Source
						, ItemCode
						, PriceTypeId
						, ParentCodeOrder
						, CNUM
						, CNUM2
						, MNUM
						, MNUM2
						, CatCommodity
						, Module
						, Category
						, Region
						, Location
						, DisplayName
						, Curr
						, UOM
						, Frequency
						, CAST([PriceYear] AS NVARCHAR(4)) [Year]
						, PriceValue 
						FROM (
								SELECT	p.PriceYear
										, Source
										, pp.ItemCode
										, CNUM
										, CNUM2
										, MNUM
										, MNUM2
										, CatCommodity
										, Module
										, Category
										, Region
										, Location
										, DisplayName
										, Curr
										, UOM
										, Frequency
										, p.PriceTypeId
										, p.PriceValue
										, pl.ParentCode
										, pl.ParentCodeOrder
										, MinPriority = MIN(pl.ParentCodeOrder) OVER (PARTITION BY pl.ParentCode, PriceYear ORDER BY PriceYear, pl.ParentCodeOrder)
							--	FROM Argus_ProductPricesByMonth p
								FROM dbo.Argus_ProductPricesByYearConverted p
								LEFT JOIN	solomon.ParentList pl 
								ON			pl.ItemCode = p.ItemCode 
								AND			pl.PriceTypeID = p.PriceTypeId
								LEFT JOIN	dbo.Product_Descriptions pp
								ON			p.ItemCode = pp.ItemCode
								AND			pp.ParentCodeOrder = 1
						) a
						WHERE ParentCodeOrder = MinPriority AND ParentCodeOrder = 1
				) T
				PIVOT(SUM(PriceValue) FOR [Year] IN ([2015],[2016],[2017],[2018],[2019],[2020],[2021],[2022],[2023],[2024],[2025])) PT
				
				UNION ALL

				SELECT	ParentCode
						, Source
						, ItemCode
						,CONVERT(varchar(3),PriceTypeId) AS PriceTypeID
						, ParentCodeOrder
						, CNUM
						, CNUM2
						, MNUM
						, MNUM2
						, CatCommodity
						, Module
						, Category
						, Region
						, Location
						, DisplayName
						, Curr
						, UOM
						, Frequency
						, ISNULL([2015],0) AS [2015]
						, ISNULL([2016],0) AS [2016]
						, ISNULL([2017],0) AS [2017]
						, ISNULL([2018],0) AS [2018]
						, ISNULL([2019],0) AS [2019]
						, ISNULL([2020],0) AS [2020]
						, ISNULL([2021],0) AS [2021]
						, ISNULL([2022],0) AS [2022]
						, ISNULL([2023],0) AS [2023]
						, ISNULL([2024],0) AS [2024]
						, ISNULL([2025],0) AS [2025]
				FROM (
				SELECT ParentCode
						, Source
						, ItemCode
						, PriceTypeId
						, ParentCodeOrder
						, CNUM
						, CNUM2
						, MNUM
						, MNUM2
						, CatCommodity
						, Module
						, Category
						, Region
						, Location
						, DisplayName
						, Curr
						, UOM
						, Frequency
						, CAST([PriceYear] AS NVARCHAR(4)) [Year]
						, PriceValue 
						FROM (
								SELECT	p.PriceYear
										, Source
										, pp.ItemCode
										, CNUM
										, CNUM2
										, MNUM
										, MNUM2
										, CatCommodity
										, Module
										, Category
										, Region
										, Location
										, DisplayName
										, Curr
										, UOM
										, Frequency
										, p.PriceTypeId
										, p.PriceValue
										, pl.ParentCode
										, pl.ParentCodeOrder
										, MinPriority = MIN(pl.ParentCodeOrder) OVER (PARTITION BY pl.ParentCode, PriceYear ORDER BY PriceYear, pl.ParentCodeOrder)
							--	FROM Platts_ProductPricesByYear p
								FROM dbo.Platts_ProductPricesByYearConverted p
								LEFT JOIN	solomon.ParentList pl 
								ON			pl.ItemCode = p.ItemCode 
								AND			pl.PriceTypeID = p.PriceTypeId
								LEFT JOIN	dbo.Product_Descriptions pp
								ON			p.ItemCode = pp.ItemCode
								AND			pp.ParentCodeOrder = 1
						) a
						WHERE ParentCodeOrder = MinPriority AND ParentCodeOrder = 1
				) T
				PIVOT(SUM(PriceValue) FOR [Year] IN ([2015],[2016],[2017],[2018],[2019],[2020],[2021],[2022],[2023],[2024],[2025])) PT
				
				
				) T1
				WHERE NOT EXISTS (	SELECT 1
							FROM  solomon.NotNeeded nn
							WHERE nn.ItemCode = T1.ItemCode)




