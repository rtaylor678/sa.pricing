﻿







CREATE VIEW [dbo].[Argus_Quotes_RolledUp2]
AS

	--ref.Argus_Quotes comes from Argus, and contains all itemcodes, including those that have been modified
	--for example, it has these two rows:
	--ItemCode	ContForwardPeriod	Timing	FwdPeriodDescription	TimeStampId	PriceTypeId	DifferentialBasis	DifferentialBasisTiming	StartDate	EndDate		OldCode	DecimalPlaces
	--PA5000005	0					prompt	na						6			3			BFO dated			NULL					2005-09-01	2007-05-31	NOCODE	2
	--PA5000005	0					prompt	na						6			3			North Sea Dated		NULL					2007-06-01	NULL		NOCODE	2
	--the first line has been superceded by the second line, with a change in the DifferentialBasis field
	--you also see that the first line has an end date, and the second line starts after that with no end date
	--it is possible to have several records for each ItemCode/PriceTypeId pair this way, but we only want one for each
	--so the following gets the latest record with a little trickery by joining them together and including where the date is < other dates, and getting the one that doesn't have an older sibling

	--and we also have to partition because of multiple PriceTypeIds

	--SELECT DISTINCT ItemCode, ContForwardPeriod, Timing, FwdPeriodDescription, TimeStampId, DifferentialBasis, DifferentialBasisTiming, StartDate, EndDate, OldCode, DecimalPlaces,
	--	PriceTypeId = CASE WHEN PriceTypeId = 2 THEN 1 ELSE PriceTypeId END
	--	--PriceTypeId = MIN(PriceTypeId) OVER (PARTITION BY ItemCode, ContForwardPeriod, Timing, FwdPeriodDescription, TimeStampId, DifferentialBasis, DifferentialBasisTiming, StartDate, EndDate, OldCode, DecimalPlaces ORDER BY PriceTypeId) 
			SELECT aq1.*
			--aq1.ItemCode, aq1.ContForwardPeriod, aq1.Timing, aq1.FwdPeriodDescription, aq1.TimeStampId, PriceTypeId = MIN(aq1.PriceTypeId), aq1.DifferentialBasis, aq1.DifferentialBasisTiming,
			--aq1.StartDate, aq1.EndDate, aq1.OldCode, aq1.DecimalPlaces
			FROM ref.Argus_Quotes aq1
				LEFT JOIN ref.Argus_Quotes aq2 
					ON aq2.ItemCode = aq1.ItemCode 
						AND aq2.ContForwardPeriod = aq1.ContForwardPeriod 
						AND aq2.Timing = aq1.Timing
						AND aq2.FwdPeriodDescription = aq1.FwdPeriodDescription 
						AND aq2.TimeStampId = aq1.TimeStampId 
						AND aq2.PriceTypeId = aq1.PriceTypeId 
						AND aq1.StartDate < aq2.StartDate
			WHERE aq2.ItemCode IS NULL
			--group by aq1.ItemCode, aq1.ContForwardPeriod, aq1.Timing, aq1.FwdPeriodDescription, aq1.TimeStampId, aq1.DifferentialBasis, aq1.DifferentialBasisTiming,
			--aq1.StartDate, aq1.EndDate, aq1.OldCode, aq1.DecimalPlaces





