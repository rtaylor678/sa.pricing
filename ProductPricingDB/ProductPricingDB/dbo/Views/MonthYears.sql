﻿


CREATE VIEW [MonthYears]
AS
	SELECT DISTINCT TOP 1000 MonthYear = CONVERT(CHAR(4), PriceDate, 100) + RIGHT((CONVERT(CHAR(4), PriceDate, 120)),2), 
		yr = year(PriceDate), 
		mt = month(PriceDate) 
	FROM ProductPrices 
	WHERE YEAR(PriceDate)>=2015 and YEAR(PriceDate)<2088
	ORDER BY yr, mt




