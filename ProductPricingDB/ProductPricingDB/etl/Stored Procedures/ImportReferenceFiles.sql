﻿

-- ---------------------------------------------------------------------------------------------
CREATE PROCEDURE [etl].[ImportReferenceFiles]
AS
BEGIN
	SET NOCOUNT ON;

		--import each of the Argus reference files we use
		--each goes into its own table, and we keep any records that exist in old files but not in the new, otherwise some of the old data won't have anything to join with
		--for each, get a temporary table with the right structure, then bulk insert the data, remove the records from the real table where they match, and insert the new records
	Print '---  Processing Argus Reference Data  ---'
		SELECT * INTO #tmpArgus_Category FROM ref.Argus_Category WHERE 1 = 0
	Print 'Inserting Argus Categories from file'
		BULK INSERT #tmpArgus_Category
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestCategory.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus Categories from table'
		DELETE FROM ref.Argus_Category WHERE ItemCode IN (SELECT ItemCode FROM #tmpArgus_Category)
		INSERT ref.Argus_Category SELECT * FROM #tmpArgus_Category
		DROP TABLE #tmpArgus_Category
	Print ''


		SELECT * INTO #tmpArgus_Codes FROM ref.Argus_Codes WHERE 1 = 0
	Print 'Inserting Argus Codes from file'
		BULK INSERT #tmpArgus_Codes
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestCodes.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus Categories from table'
		DELETE FROM ref.Argus_Codes WHERE ItemCode IN (SELECT ItemCode FROM #tmpArgus_Codes)
		INSERT ref.Argus_Codes SELECT * FROM #tmpArgus_Codes
		DROP TABLE #tmpArgus_Codes
	Print ''


		SELECT * INTO #tmpArgus_ModuleDetails FROM ref.Argus_ModuleDetails WHERE 1 = 0
	Print 'Inserting Argus ModuleDetails from file'
		BULK INSERT #tmpArgus_ModuleDetails
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestModuleDetails.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus ModuleDetails from table'
		DELETE FROM ref.Argus_ModuleDetails WHERE ItemCode IN (SELECT ItemCode FROM #tmpArgus_ModuleDetails)
		INSERT ref.Argus_ModuleDetails SELECT * FROM #tmpArgus_ModuleDetails
		DROP TABLE #tmpArgus_ModuleDetails
	Print ''


		SELECT * INTO #tmpArgus_Modules FROM ref.Argus_Modules WHERE 1 = 0
	Print 'Inserting Argus ModuleDetails from file'
		BULK INSERT #tmpArgus_Modules
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestModules.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus Categories from table'
		DELETE FROM ref.Argus_Modules WHERE Module IN (SELECT Module FROM #tmpArgus_Modules)
		INSERT ref.Argus_Modules SELECT * FROM #tmpArgus_Modules
		DROP TABLE #tmpArgus_Modules
	Print ''


		SELECT * INTO #tmpArgus_PriceType FROM ref.Argus_PriceType WHERE 1 = 0
	Print 'Inserting Argus PriceType from file'
		BULK INSERT #tmpArgus_PriceType
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestPriceType.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus PriceType from table'
		DELETE FROM ref.Argus_PriceType WHERE PriceTypeId IN (SELECT PriceTypeId FROM #tmpArgus_PriceType)
		INSERT ref.Argus_PriceType SELECT * FROM #tmpArgus_PriceType
		DROP TABLE #tmpArgus_PriceType
	Print ''


		SELECT * INTO #tmpArgus_Quotes FROM ref.Argus_Quotes WHERE 1 = 0
	Print 'Inserting Argus Quotes from file'
		BULK INSERT #tmpArgus_Quotes
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestQuotes.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus Quotes from table'
		DELETE FROM ref.Argus_Quotes WHERE ItemCode IN (SELECT ItemCode FROM #tmpArgus_Quotes)
		INSERT ref.Argus_Quotes SELECT * FROM #tmpArgus_Quotes
		DROP TABLE #tmpArgus_Quotes
	Print ''


		SELECT * INTO #tmpArgus_TimeStamp FROM ref.Argus_TimeStamp WHERE 1 = 0
	Print 'Inserting Argus TimeStamp from file'
		BULK INSERT #tmpArgus_TimeStamp
		FROM 'C:\FtpPricing\FTP\Argus\DOCUMENTATION\latestTimeStamp.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);
	Print 'Deleting duplicate Argus TimeStamp from table'
		DELETE FROM ref.Argus_TimeStamp WHERE TimeStampId IN (SELECT TimeStampId FROM #tmpArgus_TimeStamp)
		INSERT ref.Argus_TimeStamp SELECT * FROM #tmpArgus_TimeStamp
		DROP TABLE #tmpArgus_TimeStamp
	Print ''



		--Platts Categories come in a single field with multiple delimiters, so we have to do a little more work
		--start by dropping the existing table, creating a new table, and adding the data to the single column
		--then add new columns, and substring out the single column into those

	Print '---  Processing Platts Reference Data  ---'

		DROP TABLE ref.Platts_Categories

		CREATE TABLE [ref].[Platts_Categories](
			[Module] [varchar](20) NOT NULL,
			[ImportedRow] [varchar](128) NOT NULL)

		BULK INSERT ref.Platts_Categories
		FROM 'C:\FtpPricing\FTP\Platts\Symbols\csv-version\data_category_descriptions.csv'
		WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 3);

		----ALTER TABLE ref.Platts_Categories ADD Module varchar(20)
		ALTER TABLE ref.Platts_Categories ADD Category varchar(128)
		ALTER TABLE ref.Platts_Categories ADD CategoryGroup varchar(128)

		--UPDATE ref.Platts_Categories SET Module = SUBSTRING(ImportedRow, 1, CHARINDEX(' ',ImportedRow))
		UPDATE ref.Platts_Categories SET Category = ImportedRow WHERE CHARINDEX(':',ImportedRow) = 0
		UPDATE ref.Platts_Categories SET Category = SUBSTRING(ImportedRow, 1, CHARINDEX(':',ImportedRow) - 1) WHERE CHARINDEX(':',ImportedRow) > 0
		UPDATE ref.Platts_Categories SET CategoryGroup = LTRIM(SUBSTRING(ImportedRow, CHARINDEX(':',ImportedRow) + 1, LEN(RTRIM(ImportedRow)) - CHARINDEX(':',ImportedRow))) WHERE CHARINDEX(':',ImportedRow) > 0


		--Platts Symbols come in many csv files
		--we get the list of files, then loop through them and import each one

			DECLARE @cmd varchar(4000),
		@FileExtentionFilter varchar(3),
		@PathToDataFiles varchar(255),
		@FullPath varchar(100)

	--get a list of csv files in the Platts csv folder			
	SET @PathToDataFiles = 'C:\FtpPricing\FTP\Platts\Symbols\csv-version\'
	
	CREATE TABLE #tmpDataFiles (FtpFullPath varchar(255))

	SET @cmd = CONCAT('INSERT INTO #tmpDataFiles ([FtpFullPath]) EXEC xp_cmdshell ''dir /b /s "', @PathToDataFiles, '*.', 'csv', '" | FIND "."'';')
	EXEC(@cmd)

	DELETE FROM #tmpDataFiles WHERE FtpFullPath LIKE '%data_category_descriptions%' OR FtpFullPath LIKE '%ThisWeek_sym%' OR FtpFullPath IS NULL --remove ones we don't want
	
	--TRUNCATE TABLE ref.Platts_Symbols
	CREATE TABLE #tmpPlatts_Symbols([MDC] [varchar](3) NULL, [Trans] [varchar](10) NULL, [Symbol] [char](7) NULL, [Bates] [char](10) NULL, [Freq] [char](10) NULL,
		[Curr] [char](10) NULL, [UOM] [varchar](10) NULL, [DEC] [varchar](10) NULL, [Conv] [varchar](10) NULL, [StarSlash] [varchar](10) NULL,
		[To_UOM] [varchar](10) NULL, [Earliest] [date] NULL, [Latest] [date] NULL, [Description] [varchar](192) NULL)

	--loop through each csv file and import the data into the ref.Platts_Symbols table
	DECLARE dataFiles CURSOR FOR 
		SELECT FtpFullPath FROM #tmpDataFiles

	OPEN dataFiles
	
	FETCH NEXT FROM dataFiles INTO @FullPath
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @cmd = 'INSERT #tmpPlatts_Symbols SELECT * FROM OPENROWSET(BULK ''' + @FullPath + ''', FORMATFILE=''C:\FtpPricing\Seed\PlattsFtp\sym.fmt'', FIRSTROW=3) as FileData'
		EXEC (@cmd)

		FETCH NEXT FROM dataFiles INTO @FullPath
	END

	CLOSE dataFiles
	DEALLOCATE dataFiles

	UPDATE #tmpPlatts_Symbols SET [Description] = REPLACE([Description], '"','') --remove double quotes around the Description field

	DELETE FROM ref.Platts_Symbols WHERE Symbol IN (SELECT Symbol FROM #tmpPlatts_Symbols)
	INSERT ref.Platts_Symbols SELECT * FROM #tmpPlatts_Symbols

	DROP TABLE #tmpDataFiles
	DROP TABLE #tmpPlatts_Symbols

	DELETE FROM dbo.Product_Descriptions

	INSERT INTO dbo.Product_Descriptions
	SELECT * FROM dbo.vwProduct_Descriptions

END -- ALTER PROCEDURE

