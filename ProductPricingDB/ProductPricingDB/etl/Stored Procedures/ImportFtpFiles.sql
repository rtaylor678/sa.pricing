﻿CREATE PROCEDURE [etl].[ImportFtpFiles]
	@FileSource varchar(10)							-- Requires 'Platts' or 'Argus'
,	@PathToDataFiles varchar(255) = NULL			-- Pass in value if different from default folders
,	@UploadFileInfo bit = 'True'					-- If 'false' then proc will not catalog filenames found in the default FTP file folders
,	@ImportFileData bit = 'True'					-- If 'false' then proc will not import data from files stored in [etl].[FtpFileList]
,	@OutputAs varchar(10) = 'datatable'				-- 'datatable' or 'confirmation'
AS
BEGIN
	SET NOCOUNT ON;

    -- ---------------------------------------------------------------------------------------------------------------------------------------------------------- --
	-- STEP 1 - Set up local variables & objects
	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------- --

		DECLARE @PathToFormatFile			varchar(255)
			,	@LoopFileName				varchar(255)
			,	@sql						varchar(4000)
			,	@cmd						varchar(4000)
			,	@FileExtentionFilter		varchar(3)
			,	@curMaxPriceID				int			= 0;
		
		DECLARE @LoopFileID					int
			,	@LoopFileType				varchar(10)
			,	@LoopFileCategory			varchar(3)
			,	@LoopFileItemsCount			int
			,	@LoopFileDate				varchar(12)
			,	@LoopFileReportingDate		varchar(8)
			,	@LoopFileHeaderRowText		varchar(100);

		IF OBJECT_ID('tempdb..#tmpDataFiles') IS NOT NULL DROP TABLE #tmpDataFiles;						-- used to loop through multiple filenames in dir folder
		IF OBJECT_ID('tempdb..#tmpIgnoredFolders') IS NOT NULL DROP TABLE #tmpIgnoredFolders;			-- used to loop through duplicate folders and move them to Archive
		IF OBJECT_ID('tempdb..#tmpNakedArgusFiles') IS NOT NULL DROP TABLE #tmpNakedArgusFiles;			-- used to loop through Argus files not already inside a dated sub-folder
				
		-- Main temporary table --------------------------------------------------------------------------------------------------------------------------------- --

			CREATE TABLE #tmpDataFiles (
				[FtpFullPath]	varchar(255),
				[FtpFolder]		varchar(8) NULL,
				[FtpFilename]	varchar(255) NULL,
				[FtpFileExt]	varchar(4) NULL
			);
		
		-- Set DEFAULT folder params ---------------------------------------------------------------------------------------------------------------------------- --

			IF (lower(@FileSource) = 'plattsHistoricalInCSV')
				BEGIN
					SET @FileExtentionFilter = 'csv';
					SET @PathToFormatFile = 'C:\FtpPricing\Seed\PlattsFtp\PlattsCSVFileFormat.xml';			--<-- Not used ... converted files to Platts .ftp file format first.
					IF (LEN(ISNULL(fn.Scrub(@PathToDataFiles),'')) = 0)
						SET @PathToDataFiles = 'C:\FtpPricing\PlattsFtp\Download\';
				END
			IF (lower(@FileSource) = 'platts') 
				BEGIN
					SET @FileExtentionFilter = 'ftp';
					SET @PathToFormatFile = 'C:\FtpPricing\Seed\PlattsFtp\PlattsFileFormat.xml';
					IF (LEN(ISNULL(fn.Scrub(@PathToDataFiles),'')) = 0)
						SET @PathToDataFiles = 'C:\FtpPricing\PlattsFtp\Download\';
				END
			IF (lower(@FileSource) = 'argus')
				BEGIN
					SET @FileExtentionFilter = 'csv';
					SET @PathToFormatFile = 'C:\FtpPricing\Seed\ArgusCsv\ArgusFileFormat.fmt';
					IF (LEN(ISNULL(fn.Scrub(@PathToDataFiles),'')) = 0)
						SET @PathToDataFiles = 'C:\FtpPricing\ArgusCsv\Download\';
				END

	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------- --
    -- STEP 2 - Get and store filenames in temp table to loop through
	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------- --

		IF (@UploadFileInfo = 'True')
		BEGIN	

		-- Import file names / path from folder structure passed in

			SET @cmd = CONCAT('INSERT INTO #tmpDataFiles ([FtpFullPath]) EXEC xp_cmdshell ''dir /b /s "', @PathToDataFiles, '*.', @FileExtentionFilter, '" | FIND "."'';');
			EXEC(@cmd);

			DELETE FROM #tmpDataFiles WHERE [FtpFullPath] IS NULL;
			DELETE FROM #tmpDataFiles WHERE [FtpFullPath] = 'File Not Found';

		-- Catch Argus files that are not currently located within a 'dated' folder. (Example: \20160902dagm.csv to \20160902\20160902dagm.csv) ----------------- --

			IF (lower(@FileSource) = 'argus')
			BEGIN
				SELECT	*
				INTO	#tmpNakedArgusFiles
				FROM	#tmpDataFiles
				WHERE CHARINDEX('\', REPLACE([FtpFullPath], @PathToDataFiles, ''), 2) = 0;


				WHILE EXISTS (SELECT TOP 1 [FtpFullPath] FROM #tmpNakedArgusFiles)
				BEGIN
					DECLARE @tmpNakedArgusFilePath varchar(255)
						,	@tmpNewArgusFileFolder varchar(8)
						,	@tmpNakedArgusFile varchar(255);					


					PRINT 'NakedArgusFilePath'
					SELECT TOP 1 @tmpNakedArgusFilePath = [FtpFullPath] FROM #tmpNakedArgusFiles;				

					PRINT 'ArgusFileFolder'
					SET @tmpNewArgusFileFolder = LEFT(REPLACE(@tmpNakedArgusFilePath, @PathToDataFiles, ''), 8);

					PRINT 'NakedArgusFile'
					SET @tmpNakedArgusFile = REPLACE(REPLACE(@tmpNakedArgusFilePath, @PathToDataFiles, ''), '\', '');

					SET @cmd = CONCAT('ROBOCOPY /MOVE ', @PathToDataFiles, ' ', @PathToDataFiles, @tmpNewArgusFileFolder, '\ "', @tmpNakedArgusFile, '" /NP /COPYALL /ZB /LOG+:C:\FtpPricing\FTP\Logs\FileMoves.', REPLACE(CONVERT(VARCHAR(10),GetDate(),120),'-','.'), '.', @FileSource, '.log' );
					PRINT @cmd;
					EXEC xp_cmdshell @cmd;

					DELETE FROM #tmpNakedArgusFiles WHERE [FtpFullPath] = @tmpNakedArgusFilePath;
					
					DELETE FROM #tmpDataFiles WHERE [FtpFullPath] = @tmpNakedArgusFilePath;

					INSERT INTO #tmpDataFiles 
						([FtpFullPath],[FtpFolder],[FtpFilename],[FtpFileExt])
					VALUES (CONCAT(@PathToDataFiles, @tmpNewArgusFileFolder, '\', @tmpNakedArgusFile)
						,	@tmpNewArgusFileFolder
						,	@tmpNakedArgusFile
						,	@FileExtentionFilter );
				END

		
			END

		-- Parse file information from path --------------------------------------------------------------------------------------------------------------------- --
			UPDATE #tmpDataFiles SET [FtpFolder]	= REVERSE(SUBSTRING(REVERSE([FtpFullPath]), CHARINDEX('\', REVERSE([FtpFullPath])) + 1, 8)) WHERE [FtpFolder] IS NULL;
			UPDATE #tmpDataFiles SET [FtpFilename]	= REVERSE(LEFT(REVERSE([FtpFullPath]), CHARINDEX('\', REVERSE([FtpFullPath])) - 1));
			UPDATE #tmpDataFiles SET [FtpFileExt]	= @FileExtentionFilter WHERE [FtpFileExt] IS NULL;	



		-- Find, then Move, previously imported files to Archive ------------------------------------------------------------------------------------------------ --
			SELECT	*
			INTO	#tmpIgnoredFolders
			FROM	#tmpDataFiles
			WHERE CONCAT([FtpFolder],[FtpFilename]) IN (SELECT CONCAT(f.FtpFolder,f.FtpFilename) FROM [etl].[FtpFileList] f WITH (TABLOCK));
		
			WHILE EXISTS (SELECT TOP 1 [FtpFolder] FROM #tmpIgnoredFolders)
			BEGIN
				DECLARE @FtpArchiveFolder	varchar(255)
					,	@FTPOrigFilePath	varchar(255)
					,	@FTPOrigFileName	varchar(100)
					,	@FTPOrigDateFolder	varchar(8);
			
				SELECT TOP 1 @FTPOrigFilePath = [FtpFullPath], @FTPOrigDateFolder = [FtpFolder], @FTPOrigFileName = [FtpFilename] FROM #tmpIgnoredFolders;
				SET @FtpArchiveFolder = REPLACE(@PathToDataFiles, '\Download\', '\Archive\');
			
				--SET @cmd = CONCAT('ROBOCOPY /MOVE ', @PathToDataFiles, @FTPOrigDateFolder, ' ', @FtpArchiveFolder, @FTPOrigDateFolder, ' "', @FTPOrigFileName, '" /NP /COPYALL /ZB /LOG+:C:\FtpPricing\FTP\Logs\FileMoves.', REPLACE(CONVERT(VARCHAR(10),GetDate(),120),'-','.'), '.', @FileSource, '.log' );
				SET @cmd = CONCAT('ROBOCOPY /MOVE ', @PathToDataFiles, @FTPOrigDateFolder, ' ', @FtpArchiveFolder, @FTPOrigDateFolder, ' /NP /COPYALL /ZB /LOG+:C:\FtpPricing\FTP\Logs\FileMoves.', REPLACE(CONVERT(VARCHAR(10),GetDate(),120),'-','.'), '.', @FileSource, '.log' );
				PRINT @cmd;
				EXEC xp_cmdshell @cmd;
			
				DELETE FROM #tmpIgnoredFolders WHERE [FtpFolder] = @FTPOrigDateFolder;
			END

		
		-- MAIN LOOP - if any rows exist ------------------------------------------------------------------------------------------------------------------------ --

			WHILE EXISTS (SELECT TOP 1 FtpFullPath FROM #tmpDataFiles)
			BEGIN	
				
				-- Remove (delete) rows where the Folder + FileName has already been logged as imported --------------------------------------------------------- --

					SET IDENTITY_INSERT [etl].[FtpFileList_Dupes] ON;

						SELECT @curMaxPriceID = ISNULL(MAX([DupeId]),0) FROM [etl].[FtpFileList_Dupes];

											

						INSERT INTO [etl].[FtpFileList_Dupes] WITH (TABLOCK) 
							([DupeId],[FtpFileSource],[FtpFullPath],[FtpFolder],[FtpFilename],[FtpFileExt])
						SELECT	'DupeId'		= @curMaxPriceID + ROW_NUMBER() OVER (ORDER BY (SELECT NULL))
							,	'FtpFileSource' = @FileSource
							,	'FtpFullPath'	= [FtpFullPath]
							,	'FtpFolder'		= [FtpFolder]
							,	'FtpFilename'	= [FtpFilename]
							,	'FtpFileExt'	= [FtpFileExt]
						FROM #tmpDataFiles
						WHERE CONCAT(FtpFolder, FtpFilename) IN (SELECT CONCAT(f.[FtpFolder], f.[FtpFilename]) FROM [etl].[FtpFileList] f WITH (TABLOCK))
						OPTION (MAXDOP 8);


						DELETE 
						FROM #tmpDataFiles
						WHERE CONCAT(FtpFolder, FtpFilename) IN (SELECT CONCAT(f.[FtpFolder], f.[FtpFilename]) FROM [etl].[FtpFileList] f WITH (TABLOCK))

					SET IDENTITY_INSERT [etl].[FtpFileList_Dupes] OFF;

				-- PUSH captured file information to production FtpFileList table. ------------------------------------------------------------------------------ --

					SET IDENTITY_INSERT [etl].[FtpFileList] ON;

						SELECT @curMaxPriceID = ISNULL(MAX([FtpFileId]),0) FROM [etl].[FtpFileList];


						INSERT INTO [etl].[FtpFileList] WITH (TABLOCK) 
							([FtpFileId],[FtpFileSource],[FtpFullPath],[FtpFolder],[FtpFilename],[FtpFileExt])
						SELECT	@curMaxPriceID + ROW_NUMBER() OVER (ORDER BY (SELECT NULL)), *
						FROM (	SELECT	'FtpFileSource'	= @FileSource
									,	'FtpFullPath'	= MAX(FtpFullPath)
									,	'FtpFolder'		= [FtpFolder]
									,	'FtpFilename'	= [FtpFilename]
									,	'FtpFileExt'	= [FtpFileExt]
								FROM #tmpDataFiles
								GROUP BY [FtpFolder],[FtpFilename],[FtpFileExt]) tmp
						OPTION (MAXDOP 8);


						DELETE 
						FROM #tmpDataFiles
						WHERE FtpFullPath IN (SELECT d.[FtpFullPath] FROM [etl].[FtpFileList_Dupes] d WITH (TABLOCK));

					SET IDENTITY_INSERT [etl].[FtpFileList] OFF;
			END
		
			-- If a duplicate file exists, set the 'orig' file id in the table for further review --------------------------------------------------------------- --

				UPDATE dupes
				SET dupes.[FtpFileId] = files.[FtpFileId]
				FROM [etl].[FtpFileList_Dupes] dupes
					INNER JOIN [etl].[FtpFileList] files ON dupes.[FtpFullPath] = files.[FtpFullPath];
	
		END

	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------- --
	-- STEP 3 - Loop through list of filenames and import data into local table
	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------- --
	
		IF (@ImportFileData = 'True')
		BEGIN

			DELETE FROM [etl].[FtpFileList] WHERE [FtpFullPath] = 'File Not Found';

			WHILE EXISTS (SELECT TOP 1 [FtpFullPath] FROM [etl].[FtpFileList] WHERE [Processed] = 0 ORDER BY [FtpFullPath])
			BEGIN
				
				SELECT TOP 1 @LoopFileID = [FtpFileId], @LoopFileName = [FtpFullPath] FROM [etl].[FtpFileList] WHERE [Processed] = 0 ORDER BY [FtpFullPath];
				
				-- ---------------------------------------------------------------------------------------------------------------------------------------------- --
				IF (@FileSource = 'plattsHistoricalInCSV')
				BEGIN
					WHILE EXISTS (SELECT TOP 1 [FtpFullPath] FROM #tmpDataFiles WHERE [FtpFullPath] LIKE '%.csv')
					BEGIN
						SELECT TOP 1 @LoopFileName = [FtpFullPath] FROM #tmpDataFiles ORDER BY [FtpFullPath];
						
						SET @sql = 'INSERT INTO [tmp].[DataFilesImport] ';
						SET @sql = @sql + 'SELECT [DataFilename] = ''' + @LoopFileName + ''', FileData.*, ''2016-06-01'', NULL, NULL, NULL, 0 ';
						SET @sql = @sql + 'FROM OPENROWSET(BULK N''' + @LoopFileName + ''', FORMATFILE=''' + @PathToFormatFile + ''', FIRSTROW=2) as FileData;';
					
						EXEC(@sql);

						DELETE FROM #tmpDataFiles WHERE [FtpFullPath] = @LoopFileName;
					END  --WHILE EXISTS()
				END --IF (@FileSource = 'plattsHistoricalInCSV')
				-- ---------------------------------------------------------------------------------------------------------------------------------------------- --

				-- ---------------------------------------------------------------------------------------------------------------------------------------------- --
				IF (@FileSource = 'platts')
				BEGIN
					-- Step 3a - PLATTS = two steps: 1) Obtain file header row info; 2) Import data rows

						TRUNCATE TABLE [tmp].[ImportFileInfo];

						-- 1a) Platts needs a temp table for its file rows since they are variable length when a correction row occurs. ------------------------- --
							
							SET @sql = '';
							SET @sql = @sql + 'INSERT INTO [tmp].[ImportFileInfo] ';
							SET @sql = @sql + 'SELECT LTRIM(RTRIM([ROWDATA])) as RowText ';
							SET @sql = @sql + 'FROM OPENROWSET(BULK N''' + @LoopFileName + ''', FORMATFILE=''' + @PathToFormatFile + ''', FIRSTROW=2, LASTROW=2) as FileData; ';
							EXEC(@sql);

						-- 1b) Capture header row(s) info to update FtpFileList table
							
							SELECT	@LoopFileHeaderRowText	= fn.Trim([RowText])
								,	@LoopFileDate			= fn.Trim(SUBSTRING([RowText], 18, 13))
								,	@LoopFileItemsCount		= fn.Trim(SUBSTRING([RowText], 31, 6))
								,	@LoopFileType			= fn.Trim(SUBSTRING([RowText], 37, 10))
								,	@LoopFileCategory		= fn.Trim(SUBSTRING([RowText], 47, 4))
							FROM [tmp].[ImportFileInfo];				

						-- 1c) Update FtpFileList table with header row information
							
							UPDATE [etl].[FtpFileList]
							SET		[ItemCount]		= @LoopFileItemsCount
								,	[FileType]		= @LoopFileType
								,	[FileCategory]	= @LoopFileCategory
								,	[HeaderRowText]	= @LoopFileHeaderRowText
								,	[Processed]		= 1
							WHERE [FtpFileId] = @LoopFileID;

						-- 2a) Bulk import data rows starting at row 3 into temp table -------------------------------------------------------------------------- --
							
							TRUNCATE TABLE [tmp].[ImportFileInfo];

							SET @sql = 'INSERT INTO [tmp].[ImportFileInfo] WITH (TABLOCK) ';
							SET @sql = @sql + 'SELECT fn.Trim([ROWDATA]) as RowText ';
							SET @sql = @sql + 'FROM OPENROWSET(BULK N''' + @LoopFileName + ''', FORMATFILE=''' + @PathToFormatFile + ''', FIRSTROW=3) as FileData; ';
							EXEC(@sql);

						-- 2c) Insert each record - parsed into field data -------------------------------------------------------------------------------------- --
						
							SELECT @curMaxPriceID = ISNULL(MAX([PriceID]),0) FROM [dbo].[ProductPrices];

							SET IDENTITY_INSERT [dbo].[ProductPrices] ON;
								INSERT INTO [dbo].[ProductPrices] WITH (TABLOCK) 
										([PriceID],[Source],[FtpFileID],[TransType],[ItemCode],[PriceTypeID],[PriceDate],[PriceValue])
								SELECT	@curMaxPriceID + ROW_NUMBER() OVER (ORDER BY (SELECT NULL)), *
								FROM (	SELECT	'Source'		= 'Platts'
											,	'FtpFileID'		= @LoopFileID
											,	'TransType'		= LEFT([ROWTEXT],1)
											,	'ItemCode'		= fn.Trim(SUBSTRING([ROWTEXT], 3, 7))
											,	'PriceTypeID'	= fn.Trim(UPPER(SUBSTRING([ROWTEXT], 10, 2)))
											,	'PriceDate'		= CONVERT(varchar(8), fn.Trim(SUBSTRING([ROWTEXT], 12, 8)), 120)
											,	'PriceValue'	= CAST(fn.Trim(SUBSTRING([ROWTEXT], 25, CASE WHEN CHARINDEX(' ', [ROWTEXT], 25) > 0 THEN CHARINDEX(' ', [ROWTEXT], 25) - 25 ELSE CASE WHEN LEFT([ROWTEXT], 1) = 'X' THEN 0 ELSE LEN([ROWTEXT]) - 24 END END )) as money)
										FROM [tmp].[ImportFileInfo] ) tmp
								OPTION (MAXDOP 8);

							SET IDENTITY_INSERT [dbo].[ProductPrices] OFF;

				END --IF (@FileSource = 'platts')
				-- ---------------------------------------------------------------------------------------------------------------------------------------------- --

				-- ---------------------------------------------------------------------------------------------------------------------------------------------- --
				IF (@FileSource = 'Argus')
				BEGIN
					-- Step 3a - Open file and get DETAIL records (row 1 = column names)

						TRUNCATE TABLE [tmp].[ImportFileInfo];
						
						-- Build Dynamic SQL statement to import the rows in the file using a bulk import OPENROWSET() command. 

							SET @sql = '';
							SET @sql = @sql + 'DECLARE @curMaxID bigint; ' + CHAR(10);
							SET @sql = @sql + 'SELECT @curMaxID = ISNULL(MAX([PriceID]),0) FROM [dbo].[ProductPrices]; ' + CHAR(10);

							SET @sql = @sql + 'SET IDENTITY_INSERT [ProductPrices] ON; ' + CHAR(10);

							SET @sql = @sql + 'INSERT INTO [dbo].[ProductPrices] WITH (TABLOCK) ';
							SET @sql = @sql + '		([PriceID],[Source],[FtpFileID],[TransType],[ItemCode],[PriceTypeID],[PriceDate],[PriceValue],[TimeStampID],[FwdPeriod],[DiffBaseRoll],[ContFwd]) ' + CHAR(10);
							SET @sql = @sql + 'SELECT [PriceID]			= @curMaxID + ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) ';
							SET @sql = @sql + '	,	''Source''			= ''Argus'' ';
							SET @sql = @sql + '	,	''FtpFileID''		= ''' + CONVERT(varchar, @LoopFileID) + ''' ';
							SET @sql = @sql + '	,	''TransType''		= [RecordStatus] ';
							SET @sql = @sql + '	,	''ItemCode''		= [Symbol] ';
							SET @sql = @sql + '	,	''PriceTypeID''		= [PriceTypeID] ';
							SET @sql = @sql + '	,	''PriceDate''		= [PriceDate] ';
							SET @sql = @sql + '	,	''PriceValue''		= [PriceValue] ';
							SET @sql = @sql + '	,	''TimeStampID''		= [TimeStampID] ';
							SET @sql = @sql + '	,	''FwdPeriod''		= [FwdPeriod] ';
							SET @sql = @sql + '	,	''DiffBaseRoll''	= [DiffBaseRoll] ';
							SET @sql = @sql + '	,	''ContFwd''			= [ContFwd] ' + CHAR(10);
							SET @sql = @sql + ' FROM OPENROWSET(BULK N''' + @LoopFileName + ''', FORMATFILE=''' + @PathToFormatFile + ''', FIRSTROW=2) as FileData; ' + CHAR(10);

							SET @sql = @sql + 'SET IDENTITY_INSERT [ProductPrices] OFF; ' + CHAR(10);
							EXEC(@sql);	
					
						-- Mark file as processed
						UPDATE	[etl].[FtpFileList] SET	[Processed]	= 1 WHERE [FtpFileId] = @LoopFileID;
								
				END --IF (@FileSource = 'Argus')
				-- ---------------------------------------------------------------------------------------------------------------------------------------------- --

			END --WHILE EXISTS()

	
		END --IF ('DO' = 'SKIP')


END -- ALTER PROCEDURE




