﻿CREATE TABLE [etl].[FtpFileList_Dupes] (
    [DupeId]        INT           IDENTITY (1, 1) NOT NULL,
    [FtpFileId]     INT           NULL,
    [FtpFileSource] VARCHAR (10)  NULL,
    [FtpFullPath]   VARCHAR (255) NULL,
    [FtpFolder]     VARCHAR (8)   NULL,
    [FtpFilename]   VARCHAR (50)  NULL,
    [FtpFileExt]    VARCHAR (4)   NULL,
    [RecCreated]    DATETIME      CONSTRAINT [DF_FtpFileList_Dupes_RecCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FtpFileList_Dupes] PRIMARY KEY CLUSTERED ([DupeId] ASC)
);

