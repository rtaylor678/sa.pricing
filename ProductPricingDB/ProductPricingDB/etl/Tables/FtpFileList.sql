﻿CREATE TABLE [etl].[FtpFileList] (
    [FtpFileId]     INT           IDENTITY (1, 1) NOT NULL,
    [FtpFileSource] VARCHAR (10)  NULL,
    [FtpFullPath]   VARCHAR (255) NULL,
    [FtpFolder]     VARCHAR (8)   NULL,
    [FtpFilename]   VARCHAR (50)  NULL,
    [FtpFileExt]    VARCHAR (4)   NULL,
    [ItemCount]     INT           NULL,
    [FileType]      VARCHAR (10)  NULL,
    [FileCategory]  VARCHAR (3)   NULL,
    [HeaderRowText] VARCHAR (100) NULL,
    [RecCreated]    DATETIME      CONSTRAINT [DF_FtpFileList_RecCreated] DEFAULT (getdate()) NOT NULL,
    [Processed]     BIT           CONSTRAINT [DF_FtpFileList_Processed] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FtpFileList] PRIMARY KEY CLUSTERED ([FtpFileId] ASC),
    CONSTRAINT [UK_Folder_Filename] UNIQUE NONCLUSTERED ([FtpFolder] ASC, [FtpFilename] ASC)
);

