﻿CREATE TABLE [solomon].[RegionLocation] (
    [ItemCode] VARCHAR (10)   NOT NULL,
    [Region]   NVARCHAR (128) NULL,
    [Location] NVARCHAR (255) NULL
);

