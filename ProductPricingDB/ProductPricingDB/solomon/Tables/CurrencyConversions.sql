﻿CREATE TABLE [solomon].[CurrencyConversions] (
    [ConversionID] INT          IDENTITY (1, 1) NOT NULL,
    [FromCurrency] VARCHAR (4)  NOT NULL,
    [FromItemCode] VARCHAR (10) NULL,
    [ToCurrency]   VARCHAR (4)  NOT NULL,
    [ToItemCode]   VARCHAR (10) NULL
);

