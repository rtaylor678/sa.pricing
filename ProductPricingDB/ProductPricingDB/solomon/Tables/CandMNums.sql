﻿CREATE TABLE [solomon].[CandMNums] (
    [ItemCode]       VARCHAR (10) NOT NULL,
    [CNUM]           VARCHAR (50) NULL,
    [MNUM]           VARCHAR (50) NULL,
    [NumDescription] VARCHAR (50) NULL,
    [CNUM2]          VARCHAR (50) NULL,
    [MNUM2]          VARCHAR (50) NULL,
    CONSTRAINT [PK_CandMNums] PRIMARY KEY CLUSTERED ([ItemCode] ASC)
);

