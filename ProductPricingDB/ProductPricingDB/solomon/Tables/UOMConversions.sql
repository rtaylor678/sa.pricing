﻿CREATE TABLE [solomon].[UOMConversions] (
    [ItemCode]       VARCHAR (10)  NOT NULL,
    [Reason]         VARCHAR (250) NULL,
    [PriceTypeID]    VARCHAR (3)   NOT NULL,
    [UOM]            VARCHAR (20)  NULL,
    [To_UOM]         VARCHAR (20)  NULL,
    [ConversionRate] FLOAT (53)    NULL,
    CONSTRAINT [PK_UOMConversions] PRIMARY KEY CLUSTERED ([ItemCode] ASC, [PriceTypeID] ASC)
);

