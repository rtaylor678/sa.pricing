﻿CREATE TABLE [solomon].[NotNeeded] (
    [ItemCode]    VARCHAR (10) NULL,
    [Reason]      VARCHAR (50) NULL,
    [PriceTypeID] VARCHAR (3)  NULL
);


GO
CREATE NONCLUSTERED INDEX [idxNotNeeded_ItemCode]
    ON [solomon].[NotNeeded]([ItemCode] ASC);

