﻿CREATE TABLE [solomon].[ParentList] (
    [ItemCode]        VARCHAR (10) NOT NULL,
    [PriceTypeID]     VARCHAR (3)  NOT NULL,
    [ParentCode]      VARCHAR (10) NULL,
    [ParentCodeOrder] INT          NULL,
    CONSTRAINT [PK_ParentList_ItemCode_ParentCodeOrder] PRIMARY KEY CLUSTERED ([ItemCode] ASC, [PriceTypeID] ASC),
    CONSTRAINT [ParentCode_ParentCodeOrder] UNIQUE NONCLUSTERED ([ParentCode] ASC, [ParentCodeOrder] ASC)
);

