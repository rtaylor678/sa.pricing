﻿-- ----------------------------------------------------------------------------------------
-- Author:      Solomon & Associates, Inc.
-- Create date: 2015-09-01 by DAS
-- Description: Trim text
-- ----------------------------------------------------------------------------------------
CREATE FUNCTION [fn].[Trim](
       @passedValue NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @returnValue NVARCHAR(MAX) -- output string

	--CHAR(9) = Tab
	--CHAR(10) = Line feed
	--CHAR(13) = Carriage return
	--CHAR(160) = Non-Breaking Space
       
	SET @returnValue = REPLACE(@passedValue, CHAR(10), '');
	SET @returnValue = REPLACE(@returnValue, CHAR(13), '');

	WHILE CHARINDEX(CHAR(9), @returnValue, 1) = 1
	BEGIN
		SELECT @returnValue = SUBSTRING(@returnValue, 2, LEN(@returnValue) - 1)
	END

	WHILE CHARINDEX(CHAR(9), @returnValue, LEN(@returnValue) - 1) > 1
	BEGIN
		SELECT @returnValue = LEFT(@returnValue, LEN(@returnValue) - 1)
	END

	SET @returnValue = LTRIM(@returnValue);
	SET @returnValue = RTRIM(@returnValue);

	IF LEN(@returnValue) < 1
		SET @returnValue = NULL;

	RETURN @returnValue;
END


