﻿

-- ------------------------------------------------------------------------------------------
-- Author:      Solomon & Associates, Inc.
-- Create date: 2015-09-28 by DAS
-- Description: Return MAX(PriceDate) 'last date a price was supplied' from Prices table.
-- ------------------------------------------------------------------------------------------
CREATE FUNCTION [fn].[MAXPriceDate](
	@passedItemCode VARCHAR(10)
,	@passedPriceTypeID VARCHAR(2)
)
RETURNS DATE
AS
BEGIN
	DECLARE @returnValue DATE;

	SELECT @returnValue = MAX([PriceDate]) 
	FROM [dbo].[ProductPrices] 
	WHERE [ItemCode] = @passedItemCode
	  AND [PriceTypeID] = (CASE [Source] 
							WHEN 'Argus'  THEN [PriceTypeID]
							WHEN 'Platts' THEN @passedPriceTypeID
							END )

	IF LEN(@returnValue) < 1
		SET @returnValue = NULL;
	ELSE
		SET @returnValue = CONVERT(VARCHAR(10), @returnValue, 120);		--YYYY-MM-DD

	RETURN @returnValue;
END



