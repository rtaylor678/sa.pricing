﻿
-- ---------------------------------------------------------------------------------------------
-- Author:		Solomon Associates
-- Programmer:	das
-- Create date: 16.09.01
-- Description:	Report Query to return PLATTS Pricing Data
/* Syntax:		

	EXEC [rpt].[PricingMonthly_PlattsPA];

*/
-- ---------------------------------------------------------------------------------------------
CREATE PROCEDURE [rpt].[PricingMonthly_PlattsPA]
	@CalendarYear smallint = 15
,	@MaxPriceDate date = '2016-12-31'
,	@SymbolFilter varchar(10) = null
,	@OutputAs varchar(10) = 'datatable'				-- 'datatable' or 'confirmation'
AS
BEGIN
	SET NOCount ON;
/*
		PP_FileCategoryCode	=	AE,AG,etc.
		PP_ItemTrans		= 	N (New Entries)
								F (Future Dated Entries) ('examples include ‘flow date’ power AND natural gas prices, AND some OSP’s (Official Selling Prices) FOR crude oil')
								C (Change or Correction Entries)
								D (Delete Entries)
								X (No price entry - common to ignore)

		PP_ItemBateCode		=	h (High price)
								l (Low price)
								c (Close or Mean price)
								u (Index price, or unspecified)

		PP_FileLabel		=	ASIA,ASIA_PM,EARLY_PNG,EMEA,FINAL
*/

	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 1 - Set up local variables & objects
	-- -------------------------------------------------------------------------------------------------------- --

		IF OBJECT_ID('tempdb..#MonthRankPlatts1','U') IS NOT NULL DROP TABLE #MonthRankPlatts1;
		IF OBJECT_ID('tempdb..#MonthRankPlatts2a','U') IS NOT NULL DROP TABLE #MonthRankPlatts2a;
		IF OBJECT_ID('tempdb..#MonthRankPlatts2b','U') IS NOT NULL DROP TABLE #MonthRankPlatts2b;
		IF OBJECT_ID('tempdb..#SortedPlattsAll','U') IS NOT NULL DROP TABLE #SortedPlattsAll;

	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 2 - Gather 'detail' price-records - BY 'price type grouping'
	-- -------------------------------------------------------------------------------------------------------- --
		
		-- #MonthRankPlatts1 (1of1) -- All 'single' or 'stand-alone' PriceTypeID(s) --------------------------- --
		-- ---------------------------------------------------------------------------------------------------- --

			SELECT	b.[ItemCode], 'PriceTypeID' = 'c', b.[PriceMonth], b.[PriceValue]
			INTO	#MonthRankPlatts1
			FROM (
					SELECT	'ItemCode'		= pp.[ItemCode]
						,	'PriceTypeID'		= pp.[PriceTypeID]
						,	'PriceYear'		= pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeID], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceTypeID] desc, pp.[PriceDate], pp.[TransType] asc)
					FROM [dbo].[ProductPrices] pp
					WHERE pp.[Source] = 'Platts'
					  AND LOWER(pp.[PriceTypeId]) IN ('u','c')
					  AND UPPER(pp.[TransType]) IN ('N','C')
					  AND pp.[PriceYear] IN (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					GROUP BY pp.[ItemCode], pp.[PriceTypeID], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType]
					) b 
			WHERE b.[TransTypeRank] = 1;


		-- #MonthRankPlatts2a (1of2) -- PriceTypeID(s) -- Group (h,l) ----------------------------------------- --
		-- ---------------------------------------------------------------------------------------------------- --

			SELECT	b.[ItemCode], 'PriceTypeID' = 'hl', b.[PriceMonth], b.[PriceValue]
			INTO	#MonthRankPlatts2a
			FROM (
					SELECT	'ItemCode'		= pp.[ItemCode]
						,	'PriceYear'		= pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceDate], pp.[TransType] asc)
					FROM [dbo].[ProductPrices] pp
					WHERE pp.[Source] = 'Platts'
					  AND lower(pp.[PriceTypeId]) = 'h'
					  AND upper(pp.[TransType]) IN ('N','C')
					  AND pp.[PriceYear] IN (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					GROUP BY pp.[ItemCode], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType]
					) b
			WHERE b.[ItemCode] NOT IN (SELECT DISTINCT [ItemCode] FROM #MonthRankPlatts1)
			  AND b.[TransTypeRank] = 1;
		  

		-- #MonthRankPlatts2b (2of2) -- PriceTypeID(s) -- Group (h,l) ----------------------------------------- --
		-- ---------------------------------------------------------------------------------------------------- --

			SELECT	b.[ItemCode], 'PriceTypeID' = 'hl', b.[PriceMonth], b.[PriceValue]
			INTO	#MonthRankPlatts2b
			FROM (
					SELECT	'ItemCode'		= pp.[ItemCode]
						,	'PriceYear'		= pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceDate], pp.[TransType] asc)
					FROM [dbo].[ProductPrices] pp
					WHERE pp.[Source] = 'Platts'
					  AND lower(pp.[PriceTypeId]) = 'l'
					  AND upper(pp.[TransType]) IN ('N','C')
					  AND pp.[PriceYear] IN (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					GROUP BY pp.[ItemCode], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType]
					) b
			WHERE b.[ItemCode] NOT IN (SELECT DISTINCT [ItemCode] FROM #MonthRankPlatts1)
			  AND b.[TransTypeRank] = 1;


	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 3 - AGGREGATE ALL Groups - AVERAGE() BY Group, BY Month
	-- -------------------------------------------------------------------------------------------------------- --
	
		SELECT [ItemCode], 'PriceTypeID' = cast([PriceTypeID] as varchar(2)),[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24]
		INTO #SortedPlattsAll
		FROM (	
				SELECT *
				FROM  #MonthRankPlatts1 src
				pivot ( 
					AVG([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
				) piv	
			) a;


		INSERT INTO #SortedPlattsAll ([ItemCode], [PriceTypeID], [1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
		SELECT * 
		FROM (
				SELECT [ItemCode], 'PriceTypeID' = 'hl'
					,	'1'=Avg([1]),  '2'=Avg([2]),  '3'=Avg([3]),  '4'=Avg([4]),  '5'=Avg([5]),  '6'=Avg([6]),  '7'=Avg([7]),  '8'=Avg([8]),  '9'=Avg([9]),  '10'=Avg([10]),'11'=Avg([11]),'12'=Avg([12])
					,	'13'=Avg([13]),'14'=Avg([14]),'15'=Avg([15]),'16'=Avg([16]),'17'=Avg([17]),'18'=Avg([18]),'19'=Avg([19]),'20'=Avg([20]),'21'=Avg([21]),'22'=Avg([22]),'23'=Avg([23]),'24'=Avg([24])
				FROM (
					SELECT *
					FROM (
							SELECT *
							FROM  #MonthRankPlatts2a pt
							pivot ( 
								Avg([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
							) piv
				
							UNION ALL
				
							SELECT *
							FROM  #MonthRankPlatts2b pt
							pivot ( 
								Avg([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
							) piv
						) u
					) src
				GROUP BY [ItemCode], [PriceTypeID]			
			) a;

		
		-- APPEND Watch-List ItemCodes where no price information was found for the period (15-16) -------- --
		-- ---------------------------------------------------------------------------------------------------- --

			INSERT INTO #SortedPlattsAll ([ItemCode])
				SELECT	[ItemCode]
				FROM	[ref].[WatchLists]
				WHERE	[Source] = 'Platts' AND [ItemCode] NOT IN (SELECT DISTINCT [ItemCode] FROM #SortedPlattsAll);


	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 4 - RETURN QUERY - Place values in [rpt] table prior to return;
	-- -------------------------------------------------------------------------------------------------------- --
	  	

		-- ---------------------------------------------------------------------------------------------------- --
			IF OBJECT_ID('[rpt].[PricingMonthly_Platts]','U') IS NOT NULL
			BEGIN
				TRUNCATE TABLE [rpt].[PricingMonthly_Platts];
				DROP TABLE [rpt].[PricingMonthly_Platts];
			END			
		

		-- ---------------------------------------------------------------------------------------------------- --
		
			SELECT	'Source'		= CONCAT(Items.[Source], (select ' WL*' from ref.Watchlists where itemcode = p.[ItemCode]) )
				,	'ItemCode'		= Items.[ItemCode]
				,	'PriceTypeID'	= p.[PriceTypeID]
				,	'ParentCode'	= ISNULL(Items.[ParentCode],'')
				,	'PriorityRank'	= ISNULL(Items.[PriorityRank],'')
				,	'CNUM'			= ''
				,	'MNUM'			= ''
				,	'PriceType'		= CASE p.[PriceTypeId]	WHEN 'c' THEN 'close-index' 
															WHEN 'hl' THEN 'high-low' 
															ELSE ISNULL(p.[PriceTypeId],'') END
				,	'ExcelKeyCode'	= ISNULL(Items.[ExcelKeyCode], '')
				,	'ExcelKey'		= ISNULL((SELECT [CatCommodity] FROM [ProductPricing].[ref].[Excel_Categories] WHERE [CatKey] = Items.[ExcelKeyCode]), '')
				,	'Module'		= ISNULL(Items.[Module], '')
				,	'Category'		= ISNULL(Items.[Category], '')
				,	'Region'		= ISNULL(Items.[Region], '')
				,	'Location'		= ISNULL(Items.[Location], '')				
				,	'DisplayName'	= ISNULL(Items.[DisplayName], '')
				,	'Detail'		= ISNULL(Items.[Detail], '')
				,	'CUR'			= ISNULL(Items.[Currency], '')
				,	'UOM'			= ISNULL(Items.[UOM], '')
				,	'FREQ'			= CASE	WHEN Items.[Frequency] IN ('Monthly','MA','mm') THEN 'Mo'
											WHEN Items.[Frequency] IN ('Weekly','WA','DW','wk') THEN 'Wk'
											WHEN Items.[Frequency] IN ('Daily','DA','DW','d5','d7','dd') THEN 'Da'
											WHEN Items.[Frequency] IN ('Fortnightly','fn') THEN 'Fn'
											WHEN Items.[Frequency] IN ('Quarterly','QR','qq') THEN 'Qr' 
											WHEN Items.[Frequency] IN ('Yearly','YR','Annual','Annually','yy') THEN 'Yr'
											WHEN Items.[Frequency] IN ('Bi-Weekly','BW') THEN 'Bw'
											WHEN Items.[Frequency] IN ('Bi-Monthly','Bm') THEN 'Bm'
											WHEN Items.[Frequency] IN ('Bi-Yearly','By') THEN 'By'
											WHEN Items.[Frequency] IN ('Semi-Weekly','sw') THEN 'Sw'
											WHEN Items.[Frequency] IN ('Semi-Monthly','sm') THEN 'Sm'
											WHEN Items.[Frequency] IN ('Semi-Yearly','sy') THEN 'Sy'
											WHEN Items.[Frequency] IN ('Hourly', 'hh') THEN 'Hr'
											WHEN Items.[Frequency] IN ('Intraday', 'ID') THEN 'Id'
											ELSE ISNULL(Items.[Frequency],'') END
				,	'Jan15'			= ROUND([1],2), 'Feb15'=ROUND([2],2), 'Mar15'=ROUND([3],2), 'Apr15'=ROUND([4],2), 'May15'=ROUND([5],2), 'Jun15'=ROUND([6],2), 'Jul15'=ROUND([7],2), 'Aug15'=ROUND([8],2), 'Sep15'=ROUND([9],2), 'Oct15'=ROUND([10],2),'Nov15'=ROUND([11],2),'Dec15'=ROUND([12],2)	
				,	'Jan16'			= ROUND([13],2),'Feb16'=ROUND([14],2),'Mar16'=ROUND([15],2),'Apr16'=ROUND([16],2),'May16'=ROUND([17],2),'Jun16'=ROUND([18],2),'Jul16'=ROUND([19],2),'Aug16'=ROUND([20],2),'Sep16'=ROUND([21],2),'Oct16'=ROUND([22],2),'Nov16'=ROUND([23],2),'Dec16'=ROUND([24],2)
				,	'Q1Cnt15'		= CAST(0 as int)
				,	'Q2Cnt15'		= CAST(0 as int)
				,	'Q3Cnt15'		= CAST(0 as int)
				,	'Q4Cnt15'		= CAST(0 as int)
				,	'Q1Avg15'		= CAST(0 as money)
				,	'Q2Avg15'		= CAST(0 as money)
				,	'Q3Avg15'		= CAST(0 as money)
				,	'Q4Avg15'		= CAST(0 as money)				
				,	'YTDCnt15'		= CAST(0 as int)
				,	'YTDAvg15'		= CAST(0.0000 as money)
				,	'Q1Cnt16'		= CAST(0 as int)
				,	'Q2Cnt16'		= CAST(0 as int)
				,	'Q3Cnt16'		= CAST(0 as int)
				,	'Q4Cnt16'		= CAST(0 as int)				
				,	'Q1Avg16'		= CAST(0 as money)
				,	'Q2Avg16'		= CAST(0 as money)
				,	'Q3Avg16'		= CAST(0 as money)
				,	'Q4Avg16'		= CAST(0 as money)				
				,	'YTDCnt16'		= CAST(0 as int)
				,	'YTDAvg16'		= CAST(0.0000 as money)
				,	'Note'			= ISNULL(Items.[Note],'')
				,	'NotNeeded'		= CASE Items.[NotNeeded] WHEN 'True' THEN 'x' ELSE '' END
				,	'Reason'		= ISNULL(Items.[Reason],'')
				,	'Primary'		= CASE Items.[IsPrimary] WHEN 'True' THEN 'x' ELSE '' END
				,	'Secondary'		= CASE Items.[IsSecondary] WHEN 'True' THEN 'x' ELSE '' END
				,	'Tertiary'		= CASE Items.[IsTertiary] WHEN 'True' THEN 'x' ELSE '' END
				,	'StartDate'		= ISNULL(CASE WHEN Items.[Earliest_DT] IS NULL THEN NULL
												WHEN YEAR(Items.[Earliest_DT]) >= 2015 THEN convert(varchar(10), Items.[Earliest_DT], 120) 
												ELSE 'x' END, '')
				,	'EndDate'		= ISNULL(CASE WHEN Items.[Latest_DT] IS NULL THEN NULL 
												WHEN m.[MAXPriceDate] > convert(varchar(10), Items.[Latest_DT], 120) THEN 'x'
												WHEN convert(varchar(10), dateadd(MM, -2, getdate()), 120) < convert(varchar(10), Items.[Latest_DT], 120) THEN 'x'
												ELSE convert(varchar(10), Items.[Latest_DT], 120) END, '')
				,	'Reviewed'		= CASE Items.[ExcelReviewed] WHEN 'True' THEN 'x' ELSE '' END
				,	'SortPrev'		= ISNULL([CSort2], '')
				,	'SortNew'		= ROW_NUMBER() OVER ( ORDER BY Items.[ItemCode], Items.[PriceTypeID] )
			INTO [rpt].[PricingMonthly_Platts]
			FROM [dbo].[ProductItems] Items		
				INNER JOIN #SortedPlattsAll p ON Items.[ItemCode] = p.[ItemCode]
				LEFT OUTER JOIN vw.MaxPriceDates m ON Items.[ItemCode] = m.[ItemCode] AND Items.[Source] = m.[Source]
			WHERE Items.[Source] = 'Platts'
			ORDER BY p.[ItemCode], p.[PriceTypeID];


		
		
		-- Create Counts for Q1-Q4 2015 Averages Calc
		-- ---------------------------------------------------------------------------------------------------- --

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q1Cnt15] = IIF(ISNULL([Jan15],0)=0,0,1) + IIF(ISNULL([Feb15],0)=0,0,1) + IIF(ISNULL([Mar15],0)=0,0,1)
							 
		
			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q2Cnt15] = IIF(ISNULL([Apr15],0)=0,0,1) + IIF(ISNULL([May15],0)=0,0,1) + IIF(ISNULL([Jun15],0)=0,0,1)
							 

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q3Cnt15] = IIF(ISNULL([Jul15],0)=0,0,1) + IIF(ISNULL([Aug15],0)=0,0,1) + IIF(ISNULL([Sep15],0)=0,0,1)

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q4Cnt15] = IIF(ISNULL([Oct15],0)=0,0,1) + IIF(ISNULL([Nov15],0)=0,0,1) + IIF(ISNULL([Dec15],0)=0,0,1)
		
		-- Create Counts for Q1-Q4 2016 Averages Calc
		-- ---------------------------------------------------------------------------------------------------- --

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q1Cnt16] = IIF(ISNULL([Jan16],0)=0,0,1) + IIF(ISNULL([Feb16],0)=0,0,1) + IIF(ISNULL([Mar16],0)=0,0,1)
							 
		
			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q2Cnt16] = IIF(ISNULL([Apr16],0)=0,0,1) + IIF(ISNULL([May16],0)=0,0,1) + IIF(ISNULL([Jun16],0)=0,0,1)
							 

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q3Cnt16] = IIF(ISNULL([Jul16],0)=0,0,1) + IIF(ISNULL([Aug16],0)=0,0,1) + IIF(ISNULL([Sep16],0)=0,0,1)

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q4Cnt16] = IIF(ISNULL([Oct16],0)=0,0,1) + IIF(ISNULL([Nov16],0)=0,0,1) + IIF(ISNULL([Dec16],0)=0,0,1)
		

		-- Create Counts for YTD Averages Calc
		-- ---------------------------------------------------------------------------------------------------- --

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [YTDCnt15] = IIF(ISNULL([Jan15],0)=0,0,1) + IIF(ISNULL([Feb15],0)=0,0,1) + IIF(ISNULL([Mar15],0)=0,0,1) + IIF(ISNULL([Apr15],0)=0,0,1) + IIF(ISNULL([May15],0)=0,0,1) + IIF(ISNULL([Jun15],0)=0,0,1)
							 + IIF(ISNULL([Jul15],0)=0,0,1) + IIF(ISNULL([Aug15],0)=0,0,1) + IIF(ISNULL([Sep15],0)=0,0,1) + IIF(ISNULL([Oct15],0)=0,0,1) + IIF(ISNULL([Nov15],0)=0,0,1) + IIF(ISNULL([Dec15],0)=0,0,1)
		
			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [YTDCnt16] = IIF(ISNULL([Jan16],0)=0,0,1) + IIF(ISNULL([Feb16],0)=0,0,1) + IIF(ISNULL([Mar16],0)=0,0,1) + IIF(ISNULL([Apr16],0)=0,0,1) + IIF(ISNULL([May16],0)=0,0,1) + IIF(ISNULL([Jun16],0)=0,0,1)
							 + IIF(ISNULL([Jul16],0)=0,0,1) + IIF(ISNULL([Aug16],0)=0,0,1) + IIF(ISNULL([Sep16],0)=0,0,1) + IIF(ISNULL([Oct16],0)=0,0,1) + IIF(ISNULL([Nov16],0)=0,0,1) + IIF(ISNULL([Dec16],0)=0,0,1)
	

		-- Calc Quarterly Averages
		-- ---------------------------------------------------------------------------------------------------- --
			DECLARE @zero Money = 0.0000;

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q1Avg15] = CASE WHEN ISNULL([Q1Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan15],@zero) + ISNULL([Feb15],@zero) + ISNULL([Mar15],@zero)) / [Q1Cnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q2Avg15] = CASE WHEN ISNULL([Q2Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Apr15],@zero) + ISNULL([May15],@zero) + ISNULL([Jun15],@zero)) / [Q2Cnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q3Avg15] = CASE WHEN ISNULL([Q3Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jul15],@zero) + ISNULL([Aug15],@zero) + ISNULL([Sep15],@zero)) / [Q3Cnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q4Avg15] = CASE WHEN ISNULL([Q4Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Oct15],@zero) + ISNULL([Nov15],@zero) + ISNULL([Dec15],@zero)) / [Q4Cnt15], 2) END


			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q1Avg16] = CASE WHEN ISNULL([Q1Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan16],@zero) + ISNULL([Feb16],@zero) + ISNULL([Mar16],@zero)) / [Q1Cnt16], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q2Avg16] = CASE WHEN ISNULL([Q2Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Apr16],@zero) + ISNULL([May16],@zero) + ISNULL([Jun16],@zero)) / [Q2Cnt16], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q3Avg16] = CASE WHEN ISNULL([Q3Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jul16],@zero) + ISNULL([Aug16],@zero) + ISNULL([Sep16],@zero)) / [Q3Cnt16], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [Q4Avg16] = CASE WHEN ISNULL([Q4Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Oct16],@zero) + ISNULL([Nov16],@zero) + ISNULL([Dec16],@zero)) / [Q4Cnt16], 2) END


		-- Calc YTD Averages
		-- ---------------------------------------------------------------------------------------------------- --
		
			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [YTDAvg15] = CASE WHEN ISNULL([YTDCnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan15],@zero) + ISNULL([Feb15],@zero) + ISNULL([Mar15],@zero) + ISNULL([Apr15],@zero) + ISNULL([May15],@zero) + ISNULL([Jun15],@zero)
																					+ ISNULL([Jul15],@zero) + ISNULL([Aug15],@zero) + ISNULL([Sep15],@zero) + ISNULL([Oct15],@zero) + ISNULL([Nov15],@zero) + ISNULL([Dec15],@zero)) / [YTDCnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Platts] 
			SET [YTDAvg16] = CASE WHEN ISNULL([YTDCnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan16],@zero) + ISNULL([Feb16],@zero) + ISNULL([Mar16],@zero) + ISNULL([Apr16],@zero) + ISNULL([May16],@zero) + ISNULL([Jun16],@zero)
																					+ ISNULL([Jul16],@zero) + ISNULL([Aug16],@zero) + ISNULL([Sep16],@zero) + ISNULL([Oct16],@zero) + ISNULL([Nov16],@zero) + ISNULL([Dec16],@zero)) / [YTDCnt16], 2) END


		-- Show table
		-- ---------------------------------------------------------------------------------------------------- --
			IF (@OutputAs = 'datatable')
				SELECT * FROM [rpt].[PricingMonthly_Platts] ORDER BY [ItemCode], [PriceTypeID], [PriorityRank];
			ELSE 
				SELECT 'Confirmation' = 1;


END

/*
	
	EXEC [rpt].[PricingMonthly_PlattsPA];

*/

