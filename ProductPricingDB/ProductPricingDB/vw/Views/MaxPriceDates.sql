﻿

CREATE VIEW [vw].[MaxPriceDates]
AS
	SELECT	[Source]
		,	[ItemCode]
		,	'PriceTypeID' = CASE [Source] WHEN 'Argus' THEN [PriceTypeID] WHEN 'Platts' THEN NULL END
		,	MAX([PriceDate]) AS 'MAXPriceDate'
	FROM [dbo].[ProductPrices]
	GROUP BY [Source], [ItemCode], CASE [Source] WHEN 'Argus' THEN [PriceTypeID] WHEN 'Platts' THEN NULL END


