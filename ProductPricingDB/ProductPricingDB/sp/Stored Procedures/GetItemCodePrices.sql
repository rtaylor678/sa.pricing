﻿-- ---------------------------------------------------------------------------------------------
-- Author:		Solomon Associates
-- Programmer:	das
-- Create date: 2016.09.15
-- Description:	API - Excel price detail API call.
/* Syntax:		

	EXEC sp.[GetItemCodePrices] 'PA0004006';

*/
-- ---------------------------------------------------------------------------------------------
CREATE PROCEDURE [sp].[GetItemCodePrices]
	@ItemCode varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

    -- ----------------------------------------------------------------------------
	-- STEP 1 - Set up local variables & objects
	-- ----------------------------------------------------------------------------
		
		SELECT pp.[Source], pp.[TransType], pp.[ItemCode], pp.[PriceTypeID], i.[DisplayName]
			,	'Category'  = ISNULL(i.[Category], '')
			,	'Region'	= ISNULL(i.[Region], '')
			,	'Location'  = ISNULL(i.[Location], '')
			,	pp.[PriceDate], pp.[PriceYear], pp.[PriceMonth]
			,	'PriceValue'= Round(pp.[PriceValue], 3)
			,	'Currency'  = ISNULL(i.[Currency], '')
			,	'UOM'		= ISNULL(i.[UOM], '')
			,	'Frequency'	= ISNULL(i.[Frequency], '')
			--,	'Unit'		= ISNULL(i.[Unit], '')
			,	'FtpFile'	= (SELECT [FtpFullPath] FROM etl.FtpFileList WHERE FtpFileID = pp.[FtpFileID])
		FROM [dbo].[ProductPrices] pp
			INNER JOIN [dbo].[ProductItems] i ON pp.[ItemCode] = i.[ItemCode] AND pp.[PriceTypeID] = i.[PriceTypeID]
		WHERE pp.[PriceYear] IN (2015,2016)
		  AND pp.[TransType] IN ('N','C')
		  AND pp.[ItemCode] = @ItemCode
		ORDER BY pp.[PriceDate], pp.[PriceTypeID];

END

